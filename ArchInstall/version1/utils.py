from utils import * # Из корня
# from .funcs import *

import json
from menu.light import *
from .utils import *

sett = {
    'root': None,
    'boot': None,
    'swap': False,
    'home': None,
    'win_c': None,
    'win_d': None,
    'win_e': None,
    'mirrors': False,
    'wifi': False,

    'hostname': None,
    'name': None,
    'user_pass': None,
    'root_pass': None,
    'sudo': None,

    'localtime': None,

    # 'ch_mirrors': None,

    'booter': None,
    'multiboot': False,
    'boot_disk': False,

    'multilib': None,
    'xserver': None,
    'vbox': None,
    'net': None,
    'music': None,
    'ntfs': None,
    'arhive': None,

    'wm': [],
    'dm': None,

    'progs': None,

    'brawser': None,
    'office': False,
    'zsh': None,
    'yay': None,
    'pamac': None,
    
    'myprogramms': None,
}

extProgramms = [
    'blueman',
    'htop',
    'fiezilla',
    'gwenview',
    'steam',
    'neofetch',
    'screenfetch',
    'vlc',
    'gparted',
    'telegram-desktop',
    'spectacle',
    'flameshot',
]


brawser = [
    'google-chrome',
    'firefox',
    'chromium',
    'opera ( + pepper-flash )',
]

# programms = {
#     'Интернет': {
#         'Хром': ['google-chrome',]
#     }
# }




def bool_change(b):
    return False if b else True


def lstInLst(A, B):
    return ', '.join(map(str, A)) in ', '.join(map(str, B))



###############
### Установки 
###############


### wm

kde = [
    'pacman -S  plasma plasma-meta plasma-pa plasma-desktop kde-system-meta kde-utilities-meta kio-extras kwalletmanager latte-dock  konsole  kwalletmanager --noconfirm',
]

xfse = [
    'pacman -S  xfce4  pavucontrol xfce4-goodies  --noconfirm',
]

gnome = [
    'pacman -S gnome gnome-extra  --noconfirm',
]

lxde = [
    'pacman -S lxde --noconfirm',
]

deepin = [
    'pacman -S deepin deepin-extra --noconfirm',
]

mate = [
    'pacman -S  mate mate-extra  --noconfirm',
]

lxqt = [
    'pacman -S lxqt lxqt-qtplugin lxqt-themes oxygen-icons xscreensaver --noconfirm',
]

i3 = [
    'pacman -S i3 i3-wm i3status dmenu --noconfirm',
    'pacman -Sy nitrogen  --noconfirm',
]

WM = {
    'kde': kde,
    'xfse': xfse,
    'gnome': gnome,
    'lxde': lxde,
    'deepin': deepin,
    'mate': mate,
    'lxqt': lxqt,
    'i3': i3,
}


### dm

sddm = [
    'pacman -S sddm sddm-kcm --noconfirm',
    'systemctl enable sddm.service -f',
]

lightdm = [
    'pacman -S lightdm lightdm-gtk-greeter-settings lightdm-gtk-greeter --noconfirm',
    'systemctl enable lightdm.service -f',
]

gdm = [
    'pacman -S gdm --noconfirm',
    'systemctl enable gdm.service -f',
]

DM = {
    'sddm': sddm,
    'lightdm': lightdm,
    'gdm': gdm,
}