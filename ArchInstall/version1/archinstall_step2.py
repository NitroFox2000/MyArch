from .f2 import *

myprog = []

def get_name(): return sett['name']

myprogramms = {
    'Интернет': {
        'google-chrome': ListChoise([
                f'cd /home/_',   
                f'git clone https://aur.archlinux.org/google-chrome.git',
                f'chown -R _:users /home/_/google-chrome', 
                f'chown -R _:users /home/_/google-chrome/PKGBUILD', 
                f'cd /home/_/google-chrome',  
                f'sudo -u _  makepkg -si --noconfirm',  
                f'rm -Rf /home/_/google-chrome',
            ],
            f=lambda: sett['name']), 
        'firefox': ListChoise(['pacman -S firefox firefox-i18n-ru --noconfirm',]),
        'chromium': ListChoise(['pacman -S chromium --noconfirm',]),
        'opera ( + pepper-flash )': ListChoise(['pacman -S opera pepper-flash --noconfirm ',]),
    },
    'libreoffice': ListChoise(['pacman -S libreoffice-still libreoffice-still-ru --noconfirm',]),

    'ssh': ListChoise(['pacman -S openssh --noconfirm',]),

    'ZSH': ListChoise([
        "pacman -S zsh  zsh-syntax-highlighting zsh-autosuggestions grml-zsh-config --noconfirm",
        "echo 'source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh' >> /etc/zsh/zshrc",
        "echo 'source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh' >> /etc/zsh/zshrc",
        "echo 'prompt adam2' >> /etc/zsh/zshrc",
        "chsh -s /bin/zsh",
        f"chsh -s /bin/zsh _",
    ],
    f=lambda: sett['name']),

    'AUR-HELPER (YAY)': ListChoise([
        f'cd /home/_',
        f'git clone https://aur.archlinux.org/yay.git',
        f'chown -R _:users /home/_/yay',
        f'chown -R _:users /home/_/yay/PKGBUILD ',
        f'cd /home/_/yay ',
        f'sudo -u _  makepkg -si --noconfirm ',
        f'rm -Rf /home/_/yay',
    ],
    f=lambda: sett['name']),

    'pamac-aur': ListChoise([
        f"cd /home/_",
        f"git clone https://aur.archlinux.org/libpamac-aur.git",
        f"chown -R _:users /home/_/pamac-aur",
        f"chown -R _:users /home/_/libpamac-aur/PKGBUILD ",
        f"cd /home/_/libpamac-aur",
        f"sudo -u _  makepkg -si --noconfirm ",
        f"rm -Rf /home/_/libpamac-aur",
        f"cd /home/_",
        f"git clone https://aur.archlinux.org/pamac-aur.git",
        f"chown -R _:users /home/_/pamac-aur",
        f"chown -R _:users /home/_/pamac-aur/PKGBUILD",
        f"cd /home/_/pamac-aur",
        f"sudo -u _  makepkg -si --noconfirm ",
        # f"rm -Rf /home/_/pamac-aur",
        f"rm -Rf /home/_/pamac-aur",
    ],
    f=lambda: sett['name']),

    'Поддержка Android': ListChoise(['pacman -S gvfs-mtp --noconfirm',]),
    'Поддержка Iphone': ListChoise(['pacman -S gvfs-afc --noconfirm',]),
}

def myprogrs():
    rez = ConsoleMenu(myprogramms, path=['Выбор программ',]).run_simple()
    for i in rez:
        print(f'\t rez = {i} ...')
    input('...')
    sett['myprogramms'] = rez



archinstall2 = {
    O('Базовая настрйока'): V(base),
    O('localtime'): V(localtime),

    O('Загрузчик'): {
        O('Тип загрузчика'): V(booter),
        O('Мультибут', f=lambda: sett['multiboot'], ): V(multiboot),
    },

    O('Настраиваем Arch'): {
        O('multilib', f=lambda: sett['multilib']): V(multilib),
        O('X-сервер', f=lambda: sett['xserver']): V(xserver),
        O('Виртуальная машина?', f=lambda: sett['vbox']): V(vbox),
        O('NetworkManager', f=lambda: sett['net']): V(net),
        O('Поддержка звука', f=lambda: sett['music']): V(music),
        O('ntfs и fat', f=lambda: sett['ntfs']): V(ntfs),
        O('Архивы', f=lambda: sett['arhive']): V(arhive),
    },

    O('Окружение'): {
        O('Окружение рабочего стола'): V(wm),
        O('dm'): V(dm)
    },

    O('Доп программы'): V(programms),

    # O('Конечная настройка системы'): {
    #     O('Установка браузера'): V(addBrawser),
    #     O('office', f=lambda: sett['office']): V(office),
    #     O('zsh', f=lambda: sett['zsh']): V(zsh),
    #     O('yay', f=lambda: sett['yay']): V(yay),
    #     O('pamac', f=lambda: sett['pamac']): V(pamac),
    # },

    O('Выбор програм возможен только ПОСЛЕ базовой настройки системы', blank=True): None,

    O('Выбор моих программ'): V(myprogrs),
}