from menu.light import *
from . import installer
from .funcs import *
from .archinstall_step2 import archinstall2



config = {
    'Настройки': {
        O('Конфигурация и сохранения', blank=True): None,
        
        O('Сохранить конфигурацию'): V(jsave),
        O('Загрузить конфигурацию'): V(jload),
    
        O('Другое', blank=True): None,
        
        O('Какая у меня система?'): MenuSett(msg=system),
        O('Приступить к установке первого этапа'): V(f=installer.archLinuxInstall, attr=sett, back_screen=True),
        O('Приступить к установке второго этапа'): V(f=installer.installer2, attr=sett, back_screen=True),
        O('Показать выбранные варианты'): V(f=print_dict, attr=sett),
    }
}

# Добавить 
step1 = {
    O('Разметка диска (root) ', f=lambda: sett['root']): {
        'Разметить': disk_f,
        'Выбрать разделы': {
            O('*root', f=lambda: sett['root']): V(disk_root),
            O('boot', f=lambda: sett['boot']): {
                'Указать': V(disk_boot),
                'Сбросить': V(disk_boot_reset),
            },
            O('swap', f=lambda: sett['swap']): {
                'Указать': V(disk_swap),
                'Сбросить': V(disk_swap_reset),
            },
            O('home', f=lambda: sett['home']): {
                'Указать': V(disk_home),
                'Сбросить': V(disk_home_reset),
            },
            O('* - обязательные поля.', blank=True): None
        },
    },
    O('Добавить разделы windows'): {
        O('Диск C', f=lambda: sett['win_c']): {
                'Указать': (add_win_disk, ['C',], 1),
                'Сбросить': win_C_reset,
            },
        O('Диск D', f=lambda: sett['win_d']): {
                'Указать': (add_win_disk, ['D',], 1),
                'Сбросить': win_D_reset,
            },
        O('Диск E', f=lambda: sett['win_e']): {
                'Указать': (add_win_disk, ['E',], 1),
                'Сбросить': win_E_reset,
            },
    },
    O('Сменить зеркала', f=lambda: sett['mirrors']): V(mirrors),
    O('Wifi используется? ', f=lambda: sett['wifi']): V(wifi),
}




archInstall = {
    **step1,
    
    'Этап 2': V(d={**archinstall2, **config}, msg='Начинаем этап 2'),
    
    **config,
}


"""

example = {
    K('Опция 1'): 'Прмиер',
    K('Опция 2'): K('Подопция'),
    K('Опция 3'): 'Прмиер',
    K('Опция 4'): 'Прмиер',
}

"""