from .utils import *

def base():
    msg('Используйте в имени только буквы латинского алфавита')
    sett['hostname'] = input('Введите имя компьютера: ')
    sett['name'] = input('Введите имя пользователя: ')
    sett['user_pass'] = input('Введите пароль пользователя: ')
    sett['root_pass'] = input('Введите пароль ROOT: ')
    var = ConsoleMenu([
        'с паролем',
        'без пароля',
        'Sudo не добавляем'
    ], before='Настроим Sudo?', returnkey=True).display_menu()
    if var == 1:
        sett['sudo'] = 'pass'
    elif var == 2:
        sett['sudo'] = 'nopass'
    elif var == 3:
        sett['sudo'] = None



cities = [
    'Europe/Kaliningrad',
    'Europe/Kiev',
    'urope/Kirov',
    'Europe/Minsk',
    'Europe/Moscow',
    'Europe/Samara',
    'Europe/Saratov',
    'Europe/Ulyanovsk',
]

def localtime():
    msg('Настроим localtime. Укажите ваш город')
    sett['localtime'] = ConsoleMenu(cities).display_menu()


def booter():
    msg('Если установка производиться на vds тогда grub')
    msg('Если у вас версия UEFI моложе 2013г. тогда ставьте UEFI-grub')
    sett['booter'] = ConsoleMenu([
        # 'UEFI(systemd-boot)',
        'GRUB(legacy)',
        # 'UEFI-GRUB'
    ], before='Какой загрузчик установить UEFI(systemd или GRUB) или Grub-legacy?').display_menu()
    sett['boot_disk'] = input('Укажите диск куда установить GRUB (sda/sdb): ')



def multiboot(): sett['multiboot'] = bool_change(sett['multiboot'])

def multilib(): sett['multilib'] = bool_change(sett['multilib'])
def vbox(): sett['vbox'] = bool_change(sett['vbox'])
def xserver(): sett['xserver'] = bool_change(sett['xserver'])
def net(): sett['net'] = bool_change(sett['net'])
def music(): sett['music'] = bool_change(sett['music'])
def ntfs(): sett['ntfs'] = bool_change(sett['ntfs'])
def arhive(): sett['arhive'] = bool_change(sett['arhive'])


def wm():
    wm = ConsoleMenu(WM, path=['Окружение рабочего стола',]).run_simple()
    input(f'\t wm = {wm} ...')
    sett['wm'] = wm
    



def dm():
    dm = ConsoleMenu(DM, 
        before=[
            'При установке i3  без dm, dm не ставим!!!',
            'Arch-wiki рекоендует для:',
            'kde      <-> sddm',
            'Lxqt     <-> sddm',
            'xfce(i3) <-> lightdm',
            'lxde     <-> lightdm',
            'Gnome    <-> gdm',
            'Deepin   <-> lightdm',
            'Mate     <-> lightdm',
            'Установка Менеджера входа в систему',
        ],
        # var=[],
        # tmp=[],
    ).run_simple()
    input(f'\t dm = {dm} ...')
    sett['dm'] = dm


def programms():
    progs = ConsoleMenu(extProgramms, 
                        multilist=True,
                        before='Выберите дополнительные программы').display_menu()
    
    if progs.get("ok"):
        sett['progs'] = progs.get("rez")


# def addBrawser():
#     braw = ConsoleMenu(brawser, 
#                         multilist=True,
#                         before='Установим браузер?').display_menu()
    
#     if braw.get("ok"): sett['braw'] = braw.get("rez")
#     else: sett['brawser'] = None


# def office(): sett['office'] = bool_change(sett['office'])

# def zsh(): sett['zsh'] = bool_change(sett['zsh'])

# def yay(): sett['yay'] = bool_change(sett['yay'])

# def pamac(): sett['pamac'] = bool_change(sett['pamac'])


# def myprogramms():
#     pass

