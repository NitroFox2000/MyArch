from .utils import *
from .f1 import *
from .f2 import *
from .genScript import *



config = {
    'Настройки': {
        O('Конфигурация', blank=True): None,
        
        O('Сохранить конфигурацию'): V(jsave),
        O('Загрузить конфигурацию'): V(jload),
        
        O('Скрипты', blank=True): None,
        
        O('Сохранить скрипт'): V(s_save),
        O('Загрузить скрипт'): V(s_load),
        O('Распечатать скрипты'): V(print_scripts),
    
        O('Другое', blank=True): None,
        
        O('Какая у меня система?'): MenuSett(msg=system),
        # O('Приступить к установке первого этапа'): None,
        # O('Приступить к установке второго этапа'): None,
        O('Показать выбранные варианты'): {
            O('Распечатать настройки'): V(f=print_dict, attr=sett),
            O('Распечатать текущие скрипты'): V(f=print_dict, attr=sett),
        },
    
        O('Очистить всё (и скрипты и конфиги)'): V(reset_all),

        O('формирование нового скрипта'): V(genScript),
    
        O('УСТАНОВКА', blank=True): None,
        O('Установить Arch'): V(install),
    }
}



archInstall = {
    O('__ ЭТАП 1 __', blank=True): None,
    O('Разметка диска'): V(disk_sections),
    # O('Добавить разделы windows (Пока не добавлял)'): None,
    O('Сменить зеркала', f=lambda: sett['mirrors']): V(mirrors),
    O('Wifi используется? ', f=lambda: sett['wifi']): V(wifi),
    O('Сгенерировать скрипт step1'): V(step1_finish),

    O('__ ЭТАП 2 __', blank=True): None,
    O('Имена и локаль'): V(names_locale),
    O('Загрузчик'): V(bootloader),
    O('Настройка системы'): V(arch_config),
    O('Окружение'): {
        O('Окружение рабочего стола'): V(wm),
        O('dm'): V(dm)
    },
    O('Установка программ'): V(programms),


    O('Сгенерировать скрипт step2'): V(step2_finish),
    O('Сгенерировать итоговый скрипт'): V(gen_rezult_script),

    **config
}