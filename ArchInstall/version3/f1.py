from .utils import *

def jsave():
    # Сохраняем словарь в JSON-файл
    f_name = input('Введите название файла для сохранения: ')
    with open(f'saves/{f_name}.json', "w") as json_file:
        json.dump(sett, json_file)


def jload():
    global sett
    fls = ConsoleMenu(files)
    item = fls.display_menu()
    if item:
        print(f'Выбрано: {item}')
        print(f'Текущие настройки: {sett}')
        with open(f'saves/{item}', "r") as json_file:
            for k, v in json.load(json_file).items():
                sett[k] = v
        print(f'\nНовые настройки: {sett}')


################################################################################


def disk_sections():
    logger.info('disk_sections запущен')
    if lin: os.system('lsblk')
    else: print('lsblk printed ...')

    disk = input('Выберите диск для разметки (0 чтобы пропустить) (sda/sdb): ')

    if disk !='0':
        if lin: os.system(f'cfdisk /dev/{disk}')
        else: print(f'cfdisk /dev/{disk} run ...')
        

    if lin: os.system('lsblk')
    else: print('lsblk printed ...')

    root = input('Выберите раздел для ROOT (sda5 например): ')
    boot = input('Выберите раздел для BOOT (0 чтобы пропустить) (sda5 например): ')
    swap = input('Выберите раздел для SWAP (0 чтобы пропустить) (sda5 например): ')
    home = input('Выберите раздел для HOME (0 чтобы пропустить) (sda5 например): ')

    step1.append(f'mkfs.ext4 /dev/{root} -L root')
    step1.append(f'mount /dev/{root} /mnt')

    if boot !='0':
        step1.append(f'mkfs.ext2  /dev/{boot} -L boot')
        step1.append(f'mkdir /mnt/boot')
        step1.append(f'mount /dev/{boot} /mnt/boot')

    if swap !='0':
        step1.append(f'mkswap /dev/{swap} -L swap')
        step1.append(f'swapon /dev/{swap}')

    if home !='0':
        step1.append(f'mkfs.ext2  /dev/{home} -L home')
        step1.append(f'mkdir /mnt/home')
        step1.append(f'mount /dev/{home} /mnt/home')

    # Для просмотра сохранений:
    sett['root'] = root
    sett['boot'] = boot
    sett['swap'] = swap
    sett['home'] = home
    
    config['root'] = root
    config['boot'] = boot
    config['swap'] = swap
    config['home'] = home

    input('Разметка завершена! ...')

    logger.info('disk_sections завершен')


# def mirrors(): sett['mirrors'] = bool_change(sett['mirrors'])
# def wifi(): sett['wifi'] = bool_change(sett['wifi'])

def mirrors(): config['mirrors'] = bool_change(config['mirrors'])
def wifi(): config['wifi'] = bool_change(config['wifi'])


def step1_finish():
    logger.info('Формирование 1 части скипта начато')
    if sett['mirrors']:
        logger.info('Смена зеркал обнаружена')
        step1.append(f'pacman -S reflector --noconfirm')
        step1.append(f'reflector --verbose -l 50 -p http --sort rate --save /etc/pacman.d/mirrorlist')
        step1.append(f'reflector --verbose -l 15 --sort rate --save /etc/pacman.d/mirrorlist')
    
    extwifi = ''
    if sett['wifi']:
        logger.info('Поддержка wifi обнаружена')
        extwifi = 'wpa_supplicant dialog'

    step1.append(f'pacstrap /mnt base dhcpcd linux linux-headers which netctl inetutils base-devel  wget linux-firmware  nano python3 git {extwifi}')
    step1.append('genfstab -pU /mnt >> /mnt/etc/fstab')
    logger.info('Скрипт сформирован')



################################################################################

