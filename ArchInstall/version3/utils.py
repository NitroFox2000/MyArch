from utils import *
from menu.light import *
import json

d = globals()
d["SCRIPT"] = ""

config = globals()

config = {
    'mirrors': False,
    'wifi': False
}

step1 = []
step2 = []

script = ""


sett = {
    'mirrors': False,
    'wifi': False
}



def s_save():
    f_name = input('Введите название файла для сохранения: ')
    with open(f'saves_scripts/{f_name}.sh', "w") as json_file:
        # json.dump(script, json_file)
        json.dump(d["SCRIPT"], json_file)


def s_load():
    logger.info('Запущена процедура сохранения скрипта')
    global script
    fls = ConsoleMenu(get_files('./saves_scripts'))
    item = fls.display_menu()
    if item:
        print(f'Выбрано: {item}')
        print(f'Текущий скрипт: {d["SCRIPT"]}')

        with open(f'saves/{item}', "r") as json_file:
            script = json.load(json_file)
            d["SCRIPT"] = json.load(json_file)

        print(f'Новый скрипт: {d["SCRIPT"]}')
    logger.info('Скрипт сохранен')


def reset_all():
    global sett
    global script
    sett = []
    script = []
    d['SCRIPT'] = ""


def print_scripts():
    print(step1)
    input("Нажмите Enter ...")
    print(step2)
    input("Нажмите Enter ...")
    print(script)
    input("Нажмите Enter ...")
    print('\n\n', d['SCRIPT'])
    input("Нажмите Enter ...")    



cities = [
    'Europe/Kaliningrad',
    'Europe/Kiev',
    'urope/Kirov',
    'Europe/Minsk',
    'Europe/Moscow',
    'Europe/Samara',
    'Europe/Saratov',
    'Europe/Ulyanovsk',
]


### Bootloader

grab = {
    'Муьтибут +': ListChoise([
        'pacman -S grub grub-customizer os-prober  --noconfirm',
        'echo "GRUB_DISABLE_OS_PROBER=false" >> /etc/default/grub',
        'grub-install /dev/_',
        'grub-mkconfig -o /boot/grub/grub.cfg',
        'mkinitcpio -p linux',
    ], f=lambda: sett['boot_disk']),
    'Муьтибут -': ListChoise([
        'pacman -S grub   --noconfirm',
        'grub-install /dev/_',
        'grub-mkconfig -o /boot/grub/grub.cfg',
    ], f=lambda: sett['boot_disk'])
}

bootloaders = {
    'GRUB(legacy)': grab,
}

### Настройка


archConf = {
    'Добавление мультилиба': ListChoise([
        "echo '[multilib]' >> /etc/pacman.conf",
        "echo 'Include = /etc/pacman.d/mirrorlist' >> /etc/pacman.conf"
    ]),
    'Добавление X-server-а': {
        'Виртуальная машина?': ['pacman -Sy xorg-server xorg-drivers xorg-xinit virtualbox-guest-utils --noconfirm',],
        'Не машина?': ['pacman -Sy xorg-server xorg-drivers --noconfirm',],
    },
    'NetworkManager': [
        'pacman -Sy networkmanager networkmanager-openvpn network-manager-applet ppp --noconfirm',
        'systemctl enable NetworkManager.service',
        'systemctl enable dhcpcd.service',
    ],
    'Поддержка звука': [
        'pacman -Sy pulseaudio-bluetooth alsa-utils pulseaudio-equalizer-ladspa   --noconfirm',
        'systemctl enable bluetooth.service',
    ],
    'ntfs и fat': ['pacman -Sy exfat-utils ntfs-3g   --noconfirm',],
    'Архивы': [
        'pacman -Sy unzip unrar lha ark file-roller p7zip unace lrzip --noconfirm',
    ],
}


### wm

kde = [
    'pacman -S  plasma plasma-meta plasma-pa plasma-desktop kde-system-meta kde-utilities-meta kio-extras kwalletmanager latte-dock  konsole  kwalletmanager --noconfirm',
]

xfse = [
    'pacman -S  xfce4  pavucontrol xfce4-goodies  --noconfirm',
]

gnome = [
    'pacman -S gnome gnome-extra  --noconfirm',
]

lxde = [
    'pacman -S lxde --noconfirm',
]

deepin = [
    'pacman -S deepin deepin-extra --noconfirm',
]

mate = [
    'pacman -S  mate mate-extra  --noconfirm',
]

lxqt = [
    'pacman -S lxqt lxqt-qtplugin lxqt-themes oxygen-icons xscreensaver --noconfirm',
]

i3 = [
    'pacman -S i3 i3-wm i3status dmenu --noconfirm',
    'pacman -Sy nitrogen  --noconfirm',
]

WM = {
    'kde': kde,
    'xfse': xfse,
    'gnome': gnome,
    'lxde': lxde,
    'deepin': deepin,
    'mate': mate,
    'lxqt': lxqt,
    'i3': i3,
}


### dm

sddm = [
    'pacman -S sddm sddm-kcm --noconfirm',
    'systemctl enable sddm.service -f',
]

lightdm = [
    'pacman -S lightdm lightdm-gtk-greeter-settings lightdm-gtk-greeter --noconfirm',
    'systemctl enable lightdm.service -f',
]

gdm = [
    'pacman -S gdm --noconfirm',
    'systemctl enable gdm.service -f',
]

DM = {
    'sddm': sddm,
    'lightdm': lightdm,
    'gdm': gdm,
}


### Программы


extProgramms = [
    'blueman',
    'htop',
    'fiezilla',
    'gwenview',
    'steam',
    'neofetch',
    'screenfetch',
    'vlc',
    'gparted',
    'telegram-desktop',
    'spectacle',
    'flameshot',
]

myprogramms = {
    'Интернет/': {
        'google-chrome': ListChoise([
                f'cd /home/_',   
                f'git clone https://aur.archlinux.org/google-chrome.git',
                f'chown -R _:users /home/_/google-chrome', 
                f'chown -R _:users /home/_/google-chrome/PKGBUILD', 
                f'cd /home/_/google-chrome',  
                f'sudo -u _  makepkg -si --noconfirm',  
                f'rm -Rf /home/_/google-chrome',
            ],
            f=lambda: sett['name']), 
        'firefox': ListChoise(['pacman -S firefox firefox-i18n-ru --noconfirm',]),
        'chromium': ListChoise(['pacman -S chromium --noconfirm',]),
        'opera ( + pepper-flash )': ListChoise(['pacman -S opera pepper-flash --noconfirm ',]),
    },
    'libreoffice': ListChoise(['pacman -S libreoffice-still libreoffice-still-ru --noconfirm',]),

    'ssh': ListChoise(['pacman -S openssh --noconfirm',]),

    'ZSH': ListChoise([
        "pacman -S zsh  zsh-syntax-highlighting zsh-autosuggestions grml-zsh-config --noconfirm",
        "echo 'source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh' >> /etc/zsh/zshrc",
        "echo 'source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh' >> /etc/zsh/zshrc",
        "echo 'prompt adam2' >> /etc/zsh/zshrc",
        "chsh -s /bin/zsh",
        f"chsh -s /bin/zsh _",
    ],
    f=lambda: sett['name']),

    'AUR-HELPER (YAY)': ListChoise([
        f'cd /home/_',
        f'git clone https://aur.archlinux.org/yay.git',
        f'chown -R _:users /home/_/yay',
        f'chown -R _:users /home/_/yay/PKGBUILD ',
        f'cd /home/_/yay ',
        f'sudo -u _  makepkg -si --noconfirm ',
        f'rm -Rf /home/_/yay',
    ],
    f=lambda: sett['name']),

    'pamac-aur': ListChoise([
        f"cd /home/_",
        f"git clone https://aur.archlinux.org/libpamac-aur.git",
        f"chown -R _:users /home/_/pamac-aur",
        f"chown -R _:users /home/_/libpamac-aur/PKGBUILD ",
        f"cd /home/_/libpamac-aur",
        f"sudo -u _  makepkg -si --noconfirm ",
        f"rm -Rf /home/_/libpamac-aur",
        f"cd /home/_",
        f"git clone https://aur.archlinux.org/pamac-aur.git",
        f"chown -R _:users /home/_/pamac-aur",
        f"chown -R _:users /home/_/pamac-aur/PKGBUILD",
        f"cd /home/_/pamac-aur",
        f"sudo -u _  makepkg -si --noconfirm ",
        # f"rm -Rf /home/_/pamac-aur",
        f"rm -Rf /home/_/pamac-aur",
    ],
    f=lambda: sett['name']),

    'Поддержка Android': ListChoise(['pacman -S gvfs-mtp --noconfirm',]),
    'Поддержка Iphone': ListChoise(['pacman -S gvfs-afc --noconfirm',]),
}