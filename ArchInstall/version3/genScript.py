from .utils import *


def gen_sections():
    logger.info('Генерация скрипта разметки диска')

    step1.append(f'mkfs.ext4 /dev/{config["root"]} -L root')
    step1.append(f'mount /dev/{config["root"]} /mnt')

    if config["boot"] !='0':
        step1.append(f'mkfs.ext2  /dev/{config["boot"]} -L boot')
        step1.append(f'mkdir /mnt/boot')
        step1.append(f'mount /dev/{config["boot"]} /mnt/boot')

    if config["swap"] !='0':
        step1.append(f'mkswap /dev/{config["swap"]} -L swap')
        step1.append(f'swapon /dev/{config["swap"]}')

    if config["home"] !='0':
        step1.append(f'mkfs.ext2  /dev/{config["home"]} -L home')
        step1.append(f'mkdir /mnt/home')
        step1.append(f'mount /dev/{config["home"]} /mnt/home')


def gen_step1_sett():
    if config['mirrors']:
        logger.info('Смена зеркал обнаружена')
        step1.append(f'pacman -S reflector --noconfirm')
        step1.append(f'reflector --verbose -l 50 -p http --sort rate --save /etc/pacman.d/mirrorlist')
        step1.append(f'reflector --verbose -l 15 --sort rate --save /etc/pacman.d/mirrorlist')
    
    extwifi = ''
    if config.get('wifi'):
        logger.info('Поддержка wifi обнаружена')
        extwifi = 'wpa_supplicant dialog'

    step1.append(f'pacstrap /mnt base dhcpcd linux linux-headers which netctl inetutils base-devel  wget linux-firmware  nano python3 git {extwifi}')
    step1.append('genfstab -pU /mnt >> /mnt/etc/fstab')


def gen_host_time():
    step2.append(f"echo {config['hostname']} > /etc/hostname")
    step2.append(f"ln -sf /usr/share/zoneinfo/{config['localtime']} /etc/localtime")


sudo = ConsoleMenu({
    'С паролем': ["echo '%wheel ALL=(ALL) ALL' >> /etc/sudoers",],
    'Без пароля': ["echo '%wheel ALL=(ALL) NOPASSWD: ALL' >> /etc/sudoers",],
    'Без sudo': [],
})


def genScript():
    logger.info('Начата итоговая генерация скрипта')
    logger.info('Генерация 1 скрипта начата')
    gen_sections()
    gen_step1_sett()
    logger.info('Генерация 1 скрипта закончена')

    logger.info('Генерация 2 скрипта начата')
    gen_host_time()

    step2.extend(config['bootloader'])
    step2.extend(config['config'])
    step2.extend(config['wm'])
    step2.extend(config['dm'])
    step2.extend(config['extprogs'])
    step2.extend(config['myprogs'])

    username = config["username"]
    step2.append(f'useradd -m -g users -G wheel -s /bin/bash {username}')
    step2.append(f'echo " Добавляем пароль для пользователя {username} "')
    step2.append(f'12345678 | passwd {username}')
    step2.append(f'echo " Укажите пароль для ROOT "')
    step2.append(f'1234567890 | passwd')
    step2.extend(config['su'])

    with open(f'ais/chroot.sh', "w") as file:
        for line in step2:
            file.write(line)

    step1.append('cp ais/chroot.sh /mnt')
    step1.append('arch-chroot /mnt /chroot.sh')

    with open(f'ais/arhinstall.sh', "w") as file:
        for line in step1:
            file.write(line)

    step1.append()
