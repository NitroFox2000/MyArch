from .utils import *


sudo = ConsoleMenu({
    'С паролем': ["echo '%wheel ALL=(ALL) ALL' >> /etc/sudoers",],
    'Без пароля': ["echo '%wheel ALL=(ALL) NOPASSWD: ALL' >> /etc/sudoers",],
    'Без sudo': [],
})


def names_locale():
    logger.info("Установка имен и локали")

    hostname = input('Введите имя компьютера: ')
    sett['username'] = input('Введите имя пользователя: ')

    msg('Настроим localtime. Укажите ваш город')
    localtime = ConsoleMenu(cities).display_menu()

    step2.append(f"echo {hostname} > /etc/hostname")
    step2.append(f"ln -sf /usr/share/zoneinfo/{localtime} /etc/localtime")

    # Для печати
    sett['hostname'] = hostname
    sett['localtime'] = localtime

    sett['name'] = hostname # Временная обратная совместимость. Потом оставить только username

    config['username'] = sett['username']
    config['hostname'] = hostname
    config['localtime'] = localtime


    logger.info('Имена и локаль установлены')


def bootloader():
    logger.info("Установка загрузчика")
    if lin: os.system('lsblk')
    else: print('lsblk printed ...')
    sett['boot_disk'] = input('Укажите диск куда установить GRUB (sda/sdb): ')
    s = ConsoleMenu(bootloaders, 
                    before=["Выбираем загрузчик (только 1)",
                            'Если установка производиться на vds тогда grub',
                            'Если у вас версия UEFI моложе 2013г. тогда ставьте UEFI-grub'],
                    onlyone=True).run_simple()
    step2.extend(s)
    config['boot_disk'] = sett['boot_disk']
    logger.info("Загрузчик установлен")


def arch_config():
    logger.info("Настройка системы")
    s = ConsoleMenu(archConf, 
                    before=["Дополнительная настройка системы"]).run_simple()
    step2.extend(s)
    logger.info("Настройка системы завершена")


def wm():
    logger.info("Настройка wm ...")
    wm = ConsoleMenu(WM, path=['Окружение рабочего стола',]).run_simple()
    step2.extend(wm)


def dm():
    logger.info("Настройка dm ...")
    dm = ConsoleMenu(DM, 
        before=[
            'При установке i3  без dm, dm не ставим!!!',
            'Arch-wiki рекоендует для:',
            'kde      <-> sddm',
            'Lxqt     <-> sddm',
            'xfce(i3) <-> lightdm',
            'lxde     <-> lightdm',
            'Gnome    <-> gdm',
            'Deepin   <-> lightdm',
            'Mate     <-> lightdm',
            'Установка Менеджера входа в систему',
        ]).run_simple()
    step2.extend(dm)


def programms():
    logger.info('Добавление основных программ')
    progs = ConsoleMenu(extProgramms, 
                        multilist=True,
                        before='Выберите дополнительные программы').display_menu()
    step2.extend(progs)
    
    logger.info('Добавление прочих программ')
    rez = ConsoleMenu(myprogramms, path=['Выбор программ',]).run_simple()
    step2.extend(rez)
    
    logger.info('Программы добавлены')
    


def step2_finish():
    logger.info('Формирование 2-го скрипта...')
    username = sett["username"]
    step2.append(f'useradd -m -g users -G wheel -s /bin/bash {username}')
    step2.append(f'echo " Добавляем пароль для пользователя {username} "')
    step2.append(f'passwd {username}')
    step2.append(f'echo " Укажите пароль для ROOT "')
    step2.append(f'passwd')
    
    logger.info('username добавлен. Добавление судо...')

    # su = ConsoleMenu(sudo, before='Добавим sudo?').run_simple()
    su = sudo.run_simple()

    step2.extend(su)
    logger.info('2-й скрипт сформирован')


##############


def step2_to_str():
    logger.info('chroot: переделаем step2 в скрпт...')
    s2 = ''
    for item in step2:
        s2 = s2 + item + '/n'
    return s2

    


def gen_rezult_script():
    logger.info('Формирование итогового скрипта...')
    global  script
    chroot = f' arch-chroot /mnt sh -c "$({step2_to_str()})" '
    step1.append(chroot)

    for item in step1:
        script = script + item + "\n"
        d['SCRIPT'] = d['SCRIPT'] + item + '\n'
        # SCRIPT
    logger.info('Итоговый скрипт сформирован')



def install():
    os.run(d["SCRIPT"])

