from .utils import *

def host():
    msg('Используйте в имени только буквы латинского алфавита')
    hostname = input('Введите имя компьютера: ')
    script_total.append(f'echo {hostname} > /etc/hostname')

    # sett['hostname'] = input('Введите имя компьютера: ')
    # sett['name'] = input('Введите имя пользователя: ')
    # sett['user_pass'] = input('Введите пароль пользователя: ')
    # sett['root_pass'] = input('Введите пароль ROOT: ')
    # var = ConsoleMenu([
    #     'с паролем',
    #     'без пароля',
    #     'Sudo не добавляем'
    # ], before='Настроим Sudo?', returnkey=True).display_menu()
    # if var == 1:
    #     sett['sudo'] = 'pass'
    # elif var == 2:
    #     sett['sudo'] = 'nopass'
    # elif var == 3:
    #     sett['sudo'] = None



def names():
    username = input('Введите имя пользователя: ')
    script_total.append(f"useradd -m -g users -G wheel -s /bin/bash {username}")
    script_total.append(f'echo " Добавляем пароль для пользователя {username} "')
    script_total.append(f"passwd {username}")
    script_total.append(f'echo " Укажите пароль для ROOT "')
    script_total.append(f"passwd")
    su = ConsoleMenu(
        sudo,
        before='Установим sudo?',
        path='sudo'
    ).run_simple()
    script_total.extend(su)
    sett['name']=username



# Обновлено!!!
def localtime():
    msg('Настроим localtime. Укажите ваш город')
    citi = ConsoleMenu(cities).display_menu()

    script = []

    script.append(f'ln -sf /usr/share/zoneinfo/{citi} /etc/localtime')
    # script.append(f"""
    #     echo "en_US.UTF-8 UTF-8" > /etc/locale.gen
    #     echo "ru_RU.UTF-8 UTF-8" >> /etc/locale.gen
    #     locale-gen
    #     echo 'LANG="ru_RU.UTF-8"' > /etc/locale.conf 
    #     echo "KEYMAP=ru" >> /etc/vconsole.conf
    #     echo "FONT=cyr-sun16" >> /etc/vconsole.conf
    # """)
    script.append("""echo "en_US.UTF-8 UTF-8" > /etc/locale.gen""")
    script.append("""echo "ru_RU.UTF-8 UTF-8" >> /etc/locale.gen""")
    script.append("""locale-gen""")
    script.append("""echo 'LANG="ru_RU.UTF-8"' > /etc/locale.conf """)
    script.append("""echo "KEYMAP=ru" >> /etc/vconsole.conf""")
    script.append("""echo "FONT=cyr-sun16" >> /etc/vconsole.conf""")
    script_total.extend(script)


# Обновлено!!!
def booter():
    if lin: os.system('lsblk')
    sett['boot_disk'] = input('Укажите диск куда установить GRUB (sda/sdb): ')
    s = ConsoleMenu(bootloaders, 
                    before=["Выбираем загрузчик (только 1)",
                            'Если установка производиться на vds тогда grub',
                            'Если у вас версия UEFI моложе 2013г. тогда ставьте UEFI-grub'],
                    onlyone=True).run_simple()
    script_total.extend(s)
    print(script_total)


def arch_config():
    s = ConsoleMenu(archConf, 
                    before=["Дополнительная настройка системы"]).run_simple()
    script_total.extend(s)
    print(s)


def multiboot(): sett['multiboot'] = bool_change(sett['multiboot'])

def multilib(): sett['multilib'] = bool_change(sett['multilib'])
def vbox(): sett['vbox'] = bool_change(sett['vbox'])
def xserver(): sett['xserver'] = bool_change(sett['xserver'])
def net(): sett['net'] = bool_change(sett['net'])
def music(): sett['music'] = bool_change(sett['music'])
def ntfs(): sett['ntfs'] = bool_change(sett['ntfs'])
def arhive(): sett['arhive'] = bool_change(sett['arhive'])


def wm():
    wm = ConsoleMenu(WM, path=['Окружение рабочего стола',]).run_simple()
    input(f'\t wm = {wm} ...')
    sett['wm'] = wm
    script_total.extend(wm)



def dm():
    dm = ConsoleMenu(DM, 
        before=[
            'При установке i3  без dm, dm не ставим!!!',
            'Arch-wiki рекоендует для:',
            'kde      <-> sddm',
            'Lxqt     <-> sddm',
            'xfce(i3) <-> lightdm',
            'lxde     <-> lightdm',
            'Gnome    <-> gdm',
            'Deepin   <-> lightdm',
            'Mate     <-> lightdm',
            'Установка Менеджера входа в систему',
        ],
        # var=[],
        # tmp=[],
    ).run_simple()
    input(f'\t dm = {dm} ...')
    sett['dm'] = dm
    script_total.extend(dm)


def programms():
    progs = ConsoleMenu(extProgramms, 
                        multilist=True,
                        before='Выберите дополнительные программы').display_menu()
    
    if progs.get("ok"):
        sett['progs'] = progs.get("rez")
    pass


# def addBrawser():
#     braw = ConsoleMenu(brawser, 
#                         multilist=True,
#                         before='Установим браузер?').display_menu()
    
#     if braw.get("ok"): sett['braw'] = braw.get("rez")
#     else: sett['brawser'] = None


# def office(): sett['office'] = bool_change(sett['office'])

# def zsh(): sett['zsh'] = bool_change(sett['zsh'])

# def yay(): sett['yay'] = bool_change(sett['yay'])

# def pamac(): sett['pamac'] = bool_change(sett['pamac'])


# def myprogramms():
#     pass

