from menu.light import *

fast = []

rus = [
    'loadkeys ru',
    'setfont cyr-sun16',
]

time = [
    'timedatectl set-ntp true',
]

other = [
    'echo "Server = http://mirror.yandex.ru/archlinux/\$repo/os/\$arch" > /etc/pacman.d/mirrorlist',
    'pacstrap /mnt base base-devel linux linux-firmware nano dhcpcd netctl',
    'genfstab -pU /mnt >> /mnt/etc/fstab',
]


def sections():
    os.system('lsblk')
    disk = input('Выберите диск для разметки (sda/sdb): ')
    os.system(f'cfdisk /dev/{disk}')

    root = input('Укажите ROOT раздел(sda/sdb 1.2.3.4 (sda5 например)): ')
    s = [
        f'mkfs.ext4 /dev/{root} -L root',
        f'mount /dev/{root} /mnt'
    ]
    fast.extend(s)



part2 = [
    'read -p "Введите имя компьютера: " hostname',
    'read -p "Введите имя пользователя: " username',

    'echo $hostname > /etc/hostname',
    'ln -svf /usr/share/zoneinfo/Europe/Moscow  /etc/localtime',

    'echo "en_US.UTF-8 UTF-8" > /etc/locale.gen',
    'echo "ru_RU.UTF-8 UTF-8" >> /etc/locale.gen',
    'locale-gen',
    '''echo 'LANG="ru_RU.UTF-8"' > /etc/locale.conf''',
    '''echo 'KEYMAP=ru' >> /etc/vconsole.conf''',
    "echo 'FONT=cyr-sun16' >> /etc/vconsole.conf",

    'mkinitcpio -p linux',

    'pacman -Syy',
    'pacman -S grub --noconfirm',
    'grub-install /dev/sda',
    'grub-mkconfig -o /boot/grub/grub.cfg',

    'pacman -S dialog wpa_supplicant --noconfirm',

    'useradd -m -g users -G wheel -s /bin/bash $username',
    'passwd',
    'passwd $username',
    "echo '%wheel ALL=(ALL) ALL' >> /etc/sudoers",

    # На виртуалку
    'gui_install="xorg-server xorg-drivers xorg-xinit virtualbox-guest-utils"',
    # # На пк
    # 'gui_install="xorg-server xorg-drivers xorg-xinit"',

    'pacman -S $gui_install',

    'pacman -S lxdm --noconfirm',
    'systemctl enable lxdm',

    'pacman -S ttf-liberation ttf-dejavu --noconfirm ',
    'sudo pacman -S reflector firefox firefox-i18n-ru ufw f2fs-tools dosfstools ntfs-3g alsa-lib alsa-utils file-roller p7zip unrar gvfs aspell-ru pulseaudio pavucontrol --noconfirm',
    'pacman -S i3-gaps polybar dmenu pcmanfm xterm ttf-font-awesome feh gvfs udiskie ristretto tumbler picom jq --noconfirm',
    
    'pacman -S networkmanager network-manager-applet ppp --noconfirm',
    'systemctl enable NetworkManager',

    "echo 'Установка завершена! Перезагрузите систему.'",
]


def worker(scripts):
    i = input('Are you ready (Y/n)? ')
    if i == 'y' or i == 'Y':
        for item in scripts:
            os.system(item)


def main():
    fast.extend(rus)
    fast.extend(time)
    sections()
    fast.extend(rus)
    fast.extend(rus)

    fast.append(f' arch-chroot /mnt sh -c "$({part2})" ')

    worker(fast)