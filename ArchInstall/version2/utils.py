from utils import * # Из корня
# from .funcs import *

import json
from menu.light import *
from .utils import *

sett = {
    'root': None,
    'boot': None,
    'swap': False,
    'home': None,
    'win_c': None,
    'win_d': None,
    'win_e': None,
    'mirrors': False,
    'wifi': False,

    'hostname': None,
    'name': None,
    'user_pass': None,
    'root_pass': None,
    'sudo': None,

    'localtime': None,

    # 'ch_mirrors': None,

    'booter': None,
    'multiboot': False,
    'boot_disk': False,

    'multilib': None,
    'xserver': None,
    'vbox': None,
    'net': None,
    'music': None,
    'ntfs': None,
    'arhive': None,

    'wm': [],
    'dm': None,

    'progs': None,

    'brawser': None,
    'office': False,
    'zsh': None,
    'yay': None,
    'pamac': None,
    
    'myprogramms': None,
}




brawser = [
    'google-chrome',
    'firefox',
    'chromium',
    'opera ( + pepper-flash )',
]

# programms = {
#     'Интернет': {
#         'Хром': ['google-chrome',]
#     }
# }




def bool_change(b):
    return False if b else True


def lstInLst(A, B):
    return ', '.join(map(str, A)) in ', '.join(map(str, B))



###############
### Установки 
###############


script_total = []


### localtime


cities = [
    'Europe/Kaliningrad',
    'Europe/Kiev',
    'urope/Kirov',
    'Europe/Minsk',
    'Europe/Moscow',
    'Europe/Samara',
    'Europe/Saratov',
    'Europe/Ulyanovsk',
]


### Bootloader

grab = {
    'Муьтибут +': ListChoise([
        'pacman -S grub grub-customizer os-prober  --noconfirm',
        'echo "GRUB_DISABLE_OS_PROBER=false" >> /etc/default/grub',
        'grub-install /dev/_',
        'grub-mkconfig -o /boot/grub/grub.cfg',
        'mkinitcpio -p linux',
    ], f=lambda: sett['boot_disk']),
    'Муьтибут -': ListChoise([
        'pacman -S grub   --noconfirm',
        'grub-install /dev/_',
        'grub-mkconfig -o /boot/grub/grub.cfg',
    ], f=lambda: sett['boot_disk'])
}

bootloaders = {
    'GRUB(legacy)': grab,
}




### wm


archConf = {
    'Добавление мультилиба': ListChoise([
        "echo '[multilib]' >> /etc/pacman.conf",
        "echo 'Include = /etc/pacman.d/mirrorlist' >> /etc/pacman.conf"
    ]),
    'Добавление X-server-а': {
        'Виртуальная машина?': ['pacman -Sy xorg-server xorg-drivers xorg-xinit virtualbox-guest-utils --noconfirm',],
        'Не машина?': ['pacman -Sy xorg-server xorg-drivers --noconfirm',],
    },
    'NetworkManager': [
        'pacman -Sy networkmanager networkmanager-openvpn network-manager-applet ppp --noconfirm',
        'systemctl enable NetworkManager.service',
        'systemctl enable dhcpcd.service',
    ],
    'Поддержка звука': [
        'pacman -Sy pulseaudio-bluetooth alsa-utils pulseaudio-equalizer-ladspa   --noconfirm',
        'systemctl enable bluetooth.service',
    ],
    'ntfs и fat': ['pacman -Sy exfat-utils ntfs-3g   --noconfirm',],
    'Архивы': [
        'pacman -Sy unzip unrar lha ark file-roller p7zip unace lrzip --noconfirm',
    ],
}



### wm

kde = [
    'pacman -S  plasma plasma-meta plasma-pa plasma-desktop kde-system-meta kde-utilities-meta kio-extras kwalletmanager latte-dock  konsole  kwalletmanager --noconfirm',
]

xfse = [
    'pacman -S  xfce4  pavucontrol xfce4-goodies  --noconfirm',
]

gnome = [
    'pacman -S gnome gnome-extra  --noconfirm',
]

lxde = [
    'pacman -S lxde --noconfirm',
]

deepin = [
    'pacman -S deepin deepin-extra --noconfirm',
]

mate = [
    'pacman -S  mate mate-extra  --noconfirm',
]

lxqt = [
    'pacman -S lxqt lxqt-qtplugin lxqt-themes oxygen-icons xscreensaver --noconfirm',
]

i3 = [
    'pacman -S i3 i3-wm i3status dmenu --noconfirm',
    'pacman -Sy nitrogen  --noconfirm',
]

WM = {
    'kde': kde,
    'xfse': xfse,
    'gnome': gnome,
    'lxde': lxde,
    'deepin': deepin,
    'mate': mate,
    'lxqt': lxqt,
    'i3': i3,
}


### dm

sddm = [
    'pacman -S sddm sddm-kcm --noconfirm',
    'systemctl enable sddm.service -f',
]

lightdm = [
    'pacman -S lightdm lightdm-gtk-greeter-settings lightdm-gtk-greeter --noconfirm',
    'systemctl enable lightdm.service -f',
]

gdm = [
    'pacman -S gdm --noconfirm',
    'systemctl enable gdm.service -f',
]

DM = {
    'sddm': sddm,
    'lightdm': lightdm,
    'gdm': gdm,
}


### sudo


sudo = {
    'С паролем': ["echo '%wheel ALL=(ALL) ALL' >> /etc/sudoers",],
    'Без пароля': ["echo '%wheel ALL=(ALL) NOPASSWD: ALL' >> /etc/sudoers",],
    'Без sudo': ['',],
}






###




extProgramms = [
    'blueman',
    'htop',
    'fiezilla',
    'gwenview',
    'steam',
    'neofetch',
    'screenfetch',
    'vlc',
    'gparted',
    'telegram-desktop',
    'spectacle',
    'flameshot',
]