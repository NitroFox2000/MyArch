
from .utils import *
import os

# Можно попробовать записать все команды в
# итоговый скрипт, после чего в цикле тупо все выполнить и все
script = []


def worker(scripts):
    i = input('Are you ready (Y/n)? ')

    if i == 'y' or i == 'Y':
        for item in scripts:
            # sp.run(item)
            os.system(item)


# Проверим, все ли необходимое зполнено?
def ready(sett):
    ok = True
    
    if not sett['root']: 
        print('Не заполнено: root-раздел') 
        ok = False        
    return ok


# Для паузы в выполнении
def pause(txt='pause...'): input(txt)


# Разделы дисков
def sections(root, boot, swap, home):
    print('sections...')
    script.append(f'''
        mkfs.ext4 /dev/{root} -L root
        mount /dev/{root} /mnt
    ''')
    if boot:
        script.append(f'''
            mkfs.ext2  /dev/{boot} -L boot
            mkdir /mnt/boot
            mount /dev/{boot} /mnt/boot
        ''')
    if swap:
        script.append(f'''
            mkswap /dev/{swap} -L swap
            swapon /dev/{swap}
        ''')
    if home:
        script.append(f'''
            mkfs.ext2  /dev/{home} -L home
            mkdir /mnt/home
            mount /dev/{home} /mnt/home
        ''')


# Добавить разделы Windows
def windows(C, D, E):
    print('windows...')
    if C:
        script.append(f'''
            mkdir /mnt/C 
            mount /dev/{C} /mnt/C
        ''')
    if D:
        script.append(f'''
            mkdir /mnt/D 
            mount /dev/{D} /mnt/D
        ''')
    if E:
        script.append(f'''
            mkdir /mnt/E 
            mount /dev/{E} /mnt/E
        ''')


# Зеркала. Меняем, нет?
def mirrors(mirr):
    print('mirrors...')
    if mirr:
        script.append(f'''
            pacman -S reflector --noconfirm
            reflector --verbose -l 50 -p http --sort rate --save /etc/pacman.d/mirrorlist
            reflector --verbose -l 15 --sort rate --save /etc/pacman.d/mirrorlist 
        ''')


# Поддержка wifi
def wifi(wf):
    print('wifi...')
    if wf:
        script.append(f'''
            pacstrap /mnt base linux linux-headers dhcpcd which netctl inetutils  base-devel  wget linux-firmware  nano wpa_supplicant dialog python3 git
            genfstab -pU /mnt >> /mnt/etc/fstab
        ''')
    else:
        script.append(f'''
            pacstrap /mnt base dhcpcd linux linux-headers which netctl inetutils base-devel  wget linux-firmware  nano python3 git
            genfstab -pU /mnt >> /mnt/etc/fstab
        ''')


# Подготовимся к chroot
def chroot():
    script.append(f"""
        cp -r ../MyArch /mnt
        echo 'первый этап готов '
        echo 'ARCH-LINUX chroot'
        echo 'Команды для запуска скрипта: '  
        echo '          cd MyArch' 
        echo '          python3 arch.py'   
        echo 'Там нужно будет провалиться в Этап 2, насторосить все там, и установить этап 2'          
        arch-chroot /mnt
    """)


######## Установка базовой системы
def archLinuxInstall(sett):
    if ready(sett):
        global script
        script = [] # На всякий случай обнуляем
        sp.run('ls')

        sections(sett['root'], sett['boot'], sett['swap'], sett['home'])
        windows(sett['win_c'], sett['win_d'], sett['win_e'])
        mirrors(sett['mirrors'])
        wifi(sett['wifi'])
        chroot()
        
        pause()
        for s in script:
            print(s)
        pause()
        worker(script)
    else: input('Заполните обязательные значения...')



###############################################################################


# Проверим, все ли необходимое зполнено?
def ready2(sett):
    ok = True
    if not sett['hostname']: 
        print('Не заполнено: hostname') 
        ok = False
    
    if not sett['name']: 
        print('Не заполнено: name') 
        ok = False
    
    if not sett['user_pass']: 
        print('Не заполнено: user_pass') 
        ok = False
    
    if not sett['root_pass']: 
        print('Не заполнено: root_pass') 
        ok = False
    
    if not sett['localtime']: 
        print('Не заполнено: localtime') 
        ok = False
    
    if not sett['booter']: 
        print('Не заполнено: booter') 
        ok = False
    return ok


# Базовая настройка
def base(hostname, name, userpass, rootpass, sudo):
    print('base...')
    script.append(f'echo {hostname} > /etc/hostname')
    
    # script.append(f'useradd -m -g users -G wheel -s /bin/bash {name}')

    # script.append(f'echo " Добавляем пароль для пользователя {name} "')

    # script.append(f'passwd {name}')
    
    # script.append(f'echo " Укажите пароль для ROOT "')

    # script.append(f'passwd')
    # if sudo:
    #     if sudo == 'pass':
    #         script.append("echo '%wheel ALL=(ALL) ALL' >> /etc/sudoers")
    #     else:
    #         script.append("echo '%wheel ALL=(ALL) NOPASSWD: ALL' >> /etc/sudoers")
    

    # С паролями надо еще подумать


# Локали
def localtime(citi):
    print('localtime...')
    script.append(f'ln -sf /usr/share/zoneinfo/{citi} /etc/localtime')
    script.append(f"""
        echo "en_US.UTF-8 UTF-8" > /etc/locale.gen
        echo "ru_RU.UTF-8 UTF-8" >> /etc/locale.gen
        locale-gen
        echo 'LANG="ru_RU.UTF-8"' > /etc/locale.conf 
        echo "KEYMAP=ru" >> /etc/vconsole.conf
        echo "FONT=cyr-sun16" >> /etc/vconsole.conf
    """)


# Выбор загрузчика
def booter(boot, multiboot, diskboot):
    print('booter...')

    if boot == 'UEFI(systemd-boot)':
        script.append(f"""
        """)

    elif boot == 'GRUB(legacy)':
        if multiboot:
            script.append(f"""
                grub-install /dev/{diskboot}
                grub-mkconfig -o /boot/grub/grub.cfg
                mkinitcpio -p linux
            """)
        else:
            script.append(f"""
                grub-install /dev/{diskboot}
                grub-mkconfig -o /boot/grub/grub.cfg
            """)

    elif boot == 'UEFI-GRUB':
        script.append(f"""
        """)


# Настройка Arch
def archsett(multilib, xserver, vbox, net, music, ntfs, arhive):
    print('archsett...')

    if multilib:
        script.append(f"""
            echo '[multilib]' >> /etc/pacman.conf
            echo 'Include = /etc/pacman.d/mirrorlist' >> /etc/pacman.conf
        """)

    if xserver:
        if vbox:
            script.append(f"""
                pacman -Sy xorg-server xorg-drivers xorg-xinit virtualbox-guest-utils --noconfirm
            """)
        else:
            script.append(f"""
                pacman -Sy xorg-server xorg-drivers --noconfirm
            """)

    if net:
        script.append(f"""
            pacman -Sy networkmanager networkmanager-openvpn network-manager-applet ppp --noconfirm
            systemctl enable NetworkManager.service
                      
            systemctl enable dhcpcd.service
        """)

    if music:
        script.append(f"""
            pacman -Sy pulseaudio-bluetooth alsa-utils pulseaudio-equalizer-ladspa   --noconfirm
            systemctl enable bluetooth.service
        """)

    if ntfs:
        script.append(f"""
            pacman -Sy exfat-utils ntfs-3g   --noconfirm
        """)

    if arhive:
        script.append(f"""
            pacman -Sy unzip unrar lha ark --noconfirm
            pacman -Sy unzip unrar lha file-roller p7zip unace lrzip  --noconfirm  
        """)


# Окужение
def env(wm, dm):
    print('env...')

    if wm:
        for item in wm:
            script.append(item)

    if dm:
        for item in dm:
            script.append(item)


# Программы
def programms(progs):
    print('programms...')

    for p in progs:
        script.append(f'pacman -S {p} --noconfirm')


# Финальная настрйока
def finish(programms, name, sudo):
    print('my programms...')

    for programm in programms:
        script.append(programm)
    
    script.append(f'echo " Укажите пароль для ROOT "')
    
    script.append(f'passwd')
    
    
    script.append(f'useradd -m -g users -G wheel -s /bin/bash {name}')

    script.append(f'echo " Добавляем пароль для пользователя {name} "')

    script.append(f'passwd {name}')
    if sudo:
        if sudo == 'pass':
            script.append("echo '%wheel ALL=(ALL) ALL' >> /etc/sudoers")
        else:
            script.append("echo '%wheel ALL=(ALL) NOPASSWD: ALL' >> /etc/sudoers")



######## Установка в chroot
def installer2(sett):
    if ready2(sett):
        global script
        script = [] # На всякий случай обнуляем

        base(sett['hostname'], sett['name'], sett['user_pass'], sett['root_pass'], sett['sudo'])
        
        # localtime(sett['localtime'])
        # booter(sett['booter'], sett['multiboot'], sett['boot_disk'])
        script.extend(script_total)
        
        archsett(sett['multilib'], sett['xserver'], sett['vbox'], sett['net'], sett['music'], sett['ntfs'], sett['arhive'])
        env(sett['wm'], sett['dm'])
        programms(sett['progs'])
        finish(sett['myprogramms'], sett['name'], sett['sudo'])


        pause()
        for s in script:
            print(s)
        pause()
        worker(script)
    else: input('Заполните обязательные значения...')








# def total():
#     pass


    

######## Установка в chroot
def _installer2(sett):
    if ready2(sett):
        global script
        script = [] # На всякий случай обнуляем

        base(sett['hostname'], sett['name'], sett['user_pass'], sett['root_pass'], sett['sudo'])
        localtime(sett['localtime'])
        booter(sett['booter'], sett['multiboot'], sett['boot_disk'])
        archsett(sett['multilib'], sett['xserver'], sett['vbox'], sett['net'], sett['music'], sett['ntfs'], sett['arhive'])
        env(sett['wm'], sett['dm'])
        programms(sett['progs'])
        finish(sett['myprogramms'], sett['name'], sett['sudo'])


        pause()
        for s in script:
            print(s)
        pause()
        worker(script)
    else: input('Заполните обязательные значения...')


"""
    TODO: sett можно разделить: на обязательные, и необязательные,
        а в принте показывать их оба...

"""





def test_install_2():
    print(script_total)
    pause()
    worker(script_total)






