#!/bin/bash
loadkeys ru
setfont cyr-sun16
clear

pacman -Sy git glibc python3 --noconfirm

git clone https://gitlab.com/NitroFox2000/MyArch.git

cd MyArch

python -m venv env

source enf/bin/activate

pip3 install -r req.txt

python3 arch.py