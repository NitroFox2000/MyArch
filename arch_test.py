from menu.vishhhl.addons.Menu import *
from menu.light import *

def convert_menu_dict(menu_dict):
    menu_items = []

    for item_name, item_value in menu_dict.items():
        if callable(item_value):
            menu_items.append(mLabel(text=item_name, link=item_value))
        elif isinstance(item_value, dict):
            submenu = convert_menu_dict(item_value)
            menu_items.append(mOption(text=item_name, obj_menu=submenu))

    return mLayer(*menu_items)



if __name__=='__main__':
    main_menu_items = {
        'Действие 1': None,
        'Действие 2': None,
        'Действие 3': {
            'Действие 3.1': None,
            'Действие 3.2': None,
            'Действие 3.3': {
                'Действие 3.3.1': None,
                'Действие 3.3.2': None
            }
        }
    }

    main_menu = convert_menu_dict(main_menu_items)
    main_menu.enable()


