import pprint
import subprocess as sp
import platform
import os
import sys

from log import logger

directory_path = './saves'  # Укажите путь к каталогу, который вы хотите исследовать
files_all = os.listdir(directory_path)  # Получение списка файлов и подкаталогов в указанном каталоге
files = []
for file in files_all:
    if os.path.isfile(os.path.join(directory_path, file)):  # Проверка, является ли элемент файлом (а не каталогом)
        files.append(file)


# Выкинем все файлы в директории
def get_files(folder='./saves'):
    files_all = os.listdir(folder)  # Получение списка файлов и подкаталогов в указанном каталоге
    files = []
    for file in files_all:
        if os.path.isfile(os.path.join(directory_path, file)):  # Проверка, является ли элемент файлом (а не каталогом)
            files.append(file)
    return files


system = platform.system()
lin = False
if system == "Linux": lin = True


def msg(txt, max=61, multi=False):
    border = "+" + "-" * (max + 4) + "+"

    if txt is None:
        return
    
    elif isinstance(txt, dict):
        max = 25
        border = "+" + "-" * (max) + "+"
        print(border)
        for k,v in txt.items():
            print("|" + f'{k}'.center(int((max)/2)) + "=" + f'{v}'.center(int((max)/2)) + "|")
            print(border)

    elif isinstance(txt, list):
        for item in txt:
            lines = [item[i:i+max] for i in range(0, len(item), max)]
            print(border)
            for line in lines:
                formatted_line = f"|  {line.center(max)}  |"
                print(formatted_line)
            print(border)

    elif multi:
        print(border)
        for t in txt:
            lines = [t[i:i+max] for i in range(0, len(t), max)]
            for line in lines:
                formatted_line = f"|  {line.center(max)}  |"
                print(formatted_line)
            print(border)

    else:
        lines = [txt[i:i+max] for i in range(0, len(txt), max)]
        print(border)
        for line in lines:
            formatted_line = f"|  {line.center(max)}  |"
            print(formatted_line)
        print(border)



def print_dict(txt):
    max = 25
    border = "+" + "-" * (max) + "+"
    print(border)
    for k,v in txt.items():
        print("|" + f'{k}'.center(int((max)/2)) + "=" + f'{v}'.center(int((max)/2)) + "|")
        print(border)


def lstInLst(A, B):
    return ', '.join(map(str, A)) in ', '.join(map(str, B))


def lst_rm_lst(x, y):
    # input(f'x = {x}\n y = {y}, \n x-y = {list(set(x) - set(y))}\n')
    # return list(set(x) - set(y))
    return [item for item in x if item not in y]


# def lst_rm_str(x, y):
#     # input(f'x = {x}\n y = {y},')
#     # x.remove(y)
#     # input(f' x-y = {x}\n')
#     for line in x:
#         if 
#     return x


def lst_rm_str(lst, item):
    try:
        while True:
            lst.remove(item)
    except ValueError:
        return lst