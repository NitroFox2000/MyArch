def msg(txt, max=61, multi=False):
    border = "+" + "-" * (max + 4) + "+"
    
    if multi:
        print(border)
        for t in txt:
            lines = [t[i:i+max] for i in range(0, len(t), max)]
            for line in lines:
                formatted_line = f"|  {line.center(max)}  |"
                print(formatted_line)
            print(border)
    else:
        lines = [txt[i:i+max] for i in range(0, len(txt), max)]
        print(border)
        for line in lines:
            formatted_line = f"|  {line.center(max)}  |"
            print(formatted_line)
        print(border)
