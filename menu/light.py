# -*- coding: utf-8 -*-
import os
from utils import msg, lstInLst, lst_rm_lst, lst_rm_str, logger

debug = 0

try:
    from colorama import init
    init(autoreset=True)
    from colorama import Fore, Back, Style
    color=True
    print(u'Цветовая схема не загружена. Для загрузки установите colorama')
except:
    print('Цветовая схема загружена.')
    color=False


def bool_change(b):
    return False if b else True
    


# Создание ссылок в python))
class Reference:
    def __init__(self, val):
        self._value = val # just refers to val, no copy

    def get(self):
        return self._value

    def set(self, val):
        self._value = val


class ListChoise:
    def __init__(self,
                 lst,
                 f = None,
                 char='_'):
        self.lst = lst
        self.f = f
        self.char = char

    # Заменить чар на результат функции
    def lst_refactor(self):
        '''
            lst = [
                str,
                str,
                str,
            ] - начальный список
            char - искомый чар
            f - необходимая ф-ия
        '''
        rez_lst = []
        # Рассматриваем каждую строчку
        for line in self.lst:
            rez_line = ''
            # Теперь проверяем каждый символ в строке
            for ch in line:
                # Если совпадает с искомым char
                if ch == self.char:
                    # input(f'Найдено совпадение!!!! rez_line = {rez_line}, f() = {self.f()}')
                    try:
                        rez_line = rez_line + self.f()
                    except:
                        return False
                # У нас символ, пропускаем его
                else:
                    rez_line = rez_line + ch
            rez_lst.append(rez_line)
        return rez_lst


class MenuSett:
    def __init__(self, msg=None, f=None):
        self.msg = msg
        self.f = f

# Класс-ключи
class O:
    def __init__(self, title, var=None, blank=False, color=None, f=None, addBefore=None):
        self.title = title
        self.var = var
        self.blank = blank
        self.color = color
        self.f = f
        self.addBefore = addBefore


    # Отрисовкак, кода ключ - экземпляр O
    def print_item(self, index, max_item_length, border):
        addTree = True
        # Если просто отображать...
        if self.blank:
            print("|" + f"{self.title}".center(max_item_length+5) + '|')
            print(border)
            addTree = False

        # Если есть значение...
        elif self.var is not None:
            # print(f'self = {self}')
            len_for_var = int(max_item_length - len(self.title))
            print(f"| {index}. " 
                + f"{self.title[1:]}".ljust(len(self.title)) 
                + '=' 
                + f'{self.var} '.center(len_for_var) 
                + '|')
            print(border)
            index += 1

        # Если функция
        elif self.f:
            len_for_var = int(max_item_length - len(self.title))
            print(f"| {index}. " 
                + f"{self.title}".ljust(len(self.title)+1) 
                + '=' 
                + f'{self.f()} '.rjust(len_for_var-1) 
                + '|')
            print(border)
            index += 1

        # Если просто обычный текст....
        else:  
            print(f"| {index}. {self.title}".ljust(max_item_length+5) + ' |')
            print(border)
            index += 1
        return index, addTree


    def __len__(self):
        return len(self.title)
    
    def __getitem__(self, item):
        return self.title[item] 
    
    def __str__(self):
        return str(self.title)


class OB:
    def __init__(self, msg):
        self.msg = msg


# Класс-значения
class V:
    def __init__(self,  
                 f=None, 
                 attr=None, 
                 var=False, 
                 none=False,
                 back_screen=False,
                 ref=None,
                 d=None,
                 msg=None,
                 lst=None,
                 lst_multy=False):
        self.f = f
        self.attr = attr
        self.var = var
        self.none = none
        self.back_screen = back_screen
        self.ref=ref
        self.d=d
        self.msg=msg
        self.lst=lst
        self.lst_multy=lst_multy




class ConsoleMenu:
    def __init__(self, 
                 menu_items, 
                 parent_menu=None, 
                 path=['Главное меню',], 
                 before=None, 
                 tree=None,
                 rescreen=False,
                 multilist=False,
                 returnkey=False,
                 null=True,
                 var=[],
                 tmp=[],
                 onlyone=False):
        self.menu_items = menu_items
        self.path = path
        self.parent_menu = parent_menu
        self.before = before
        self.tree = tree # Дерево без неиспользуемых элементов (blank=False)
        self.rescreen = rescreen
        self.multilist = multilist
        self.returnkey = returnkey
        self.null = null

        self.max_line = 80          # Максимальная длинна меню (ограничение)
        self.max_current = 0        # Максимальная длинна строки текущего подменю
        self.max_item_length = 0    # Максимальная длинна 
        self.border = ''

        # Тесты
        # self.var = []
        self.tmp = []
        self.var = var
        # self.tmp = tmp
        self.f_exit = False
        self.onlyone = onlyone

        self.exit = False

        logger.debug('Создан ConsoleMenu')
        logger.debug(f"self.var = {self.var}")
        logger.debug(f"self.tmp = {self.tmp}")


    def clear_screen(self):
        os.system('cls' if os.name == 'nt' else 'clear')


    # Надо бы переименовать на run_list_choise
    def run_simple(self):
        logger.debug('run_simple стартовал')
        while True:
            logger.debug('run_simple - итерация началась')
            self.countBorders()
            self.clear_screen()

            msg(self.before)

            # Подпись "меню" и хлебные крошки
            if not color:
                self.msg('Цветовая схема не загружена. Для загрузки установите colorama')
            print("+" + " Меню ".center(self.max_item_length+5, '=') + "+")
            if color:
                print("|" + Fore.RED + " > ".join(self.path).center(self.max_item_length+5, ' ') + Style.RESET_ALL + "|")
            else:
                print("|" + " > ".join(self.path).center(self.max_item_length+5) + "|")

            # Печать меню
            self.new_show()
            # Выбор элемента меню
            lst_item = self.new_choice()

            # if lst_item: return lst_item
            if isinstance(self.menu_items, list): return lst_item 
            if self.f_exit or self.exit: 
                # Если не очестить - почему-то сохраняется между разными категориями одного меню
                self.tmp = []
                # self.var = []
                logger.debug(f'run_simple: Итерация привела к выходу из ConsoleMenu.')
                logger.debug(f'run_simple: Передаваемый self.var = {self.var}')
                logger.debug(f'run_simple: self.tmp = {self.tmp}')
                return self.var 


    def new_choice(self):
        logger.debug('new_choice начался выбор')
        # Выбор меню
        ext = '(или несколько, разделяя пробелом) ' if self.multilist else ''
        print(f'now: choice = {self.tmp}')
        choice = input(f"Введите номер пункта {ext}(q для выхода/0 - закончить выбор): ")

        try:
            rez = []
            list_items = choice.split(' ')
            for item in list_items:

                # Выход
                if item == 'q':
                    ans = input("\nDo you really want to go out (Y/n) ").upper()
                    if ans == "Y":
                        exit()
            
                # Выход 2
                elif item == '0': self.f_exit = True
                
                # Если это число ...
                elif item.isdigit():
                    item = int(item)

                    # Список обрабатываем отдельно. При выборе элемента - вернуть его..
                    if isinstance(self.menu_items, list):
                        # input('Выбор в списке...')
                        if 1 <= item <= len(self.menu_items):
                            if self.returnkey:
                                return item
                            else:
                                return self.menu_items[item-1]
                        elif int(item) == 0:
                            return False
                        else:
                            input("Неверный выбор. Нажмите Enter для продолжения.")

                    # Выбран легитимный вариант меню
                    elif 1 <= item <= len(self.menu_items):
                        selected_key = list(self.menu_items.keys())[item - 1]
                        selected_item = list(self.menu_items.values())[item - 1]

                        # input(f'selected_item = {selected_item}, f = {selected_item.f}')

                        # Элемент - словарь - подменю
                        if isinstance(selected_item, dict):
                            sub_menu = ConsoleMenu(selected_item, 
                                                self, 
                                                self.path+[str(selected_key),], 
                                                before=self.before,
                                                # var=self.var,
                                                # tmp=self.tmp,
                                                onlyone=self.onlyone
                                                )
                            var_ = sub_menu.run_simple()
                            # self.var.extend(var_)
                            self.var = self.var + var_

                        # Список - пакеты для добавления
                        elif isinstance(selected_item, list):
                            if lstInLst(selected_item, self.var):
                                self.tmp = lst_rm_str(self.tmp, selected_key)
                                self.var = lst_rm_lst(self.var, selected_item)
                            else:
                                # input(f'self.var = {self.var + selected_item}') # Тут порядок сохранен еще 
                                # selected_item = self.lst_factory(selected_item)
                                self.var = self.var + selected_item
                                self.tmp.append(selected_key)
                                input(f'Пакеты для установки добавлены - {selected_key}... ')

                        # Пропытка распарсить строку динамически.... 
                        elif isinstance(selected_item, ListChoise):
                            # logger.debug('')
                            logger.debug('selected_item - это ListChoise')
                            # f = selected_item.f() if selected_item.f else selected_item.f
                            # input(f'selected_item.f() = {f}')

                            refactored_lst = selected_item.lst_refactor()
                            logger.debug(f'refactored_lst = {refactored_lst}')
                            logger.debug(f'self.var = {self.var}')
                            logger.debug(f'self.tmp = {self.tmp}')

                            if not refactored_lst:
                                self.before = 'Ошибка! Переданная функция не вызывается!'
                                # raise 'Ошибка! Переданная функция не вызывается!'

                            elif lstInLst(refactored_lst, self.var):
                                logger.warning(f'lst_rm_str: self.tmp = {self.tmp}, selected_key = {selected_key}')
                                logger.warning(f'lst_rm_lst: self.var = {self.var}, refactored_lst = {refactored_lst}')
                                self.tmp = lst_rm_str(self.tmp, selected_key)
                                self.var = lst_rm_lst(self.var, refactored_lst)
                                logger.warning(f'lst_rm_str: self.tmp = {self.tmp}, selected_key = {selected_key}')
                                logger.warning(f'lst_rm_lst: self.var = {self.var}, refactored_lst = {refactored_lst}')
                            else:
                                # refactored_lst = self.lst_factory(refactored_lst)
                                self.var = self.var + refactored_lst
                                self.tmp.append(selected_key)
                                input(f'Пакеты для установки добавлены - {selected_key}... ')
                                if self.onlyone: 
                                    self.exit = True
                                    return self.var
                                
                            logger.debug(f'После выбора')
                            logger.debug(f'self.var = {self.var}')
                            logger.debug(f'self.tmp = {self.tmp}')

                    # Выход
                    elif item == 0 and self.null:
                        self.check_parent()

                    else:
                        input("Неверный выбор. Нажмите Enter для продолжения.")
                
                else:
                    input("Введено неккоректное значение. Нажмите Enter для продолжения.")

            logger.debug(f'new_choice - Выбор окночен. Возвращаемый rez = {rez}')
            return rez
        except ZeroDivisionError:
            input('Распарсить мультивыбор не удалось...')


    def new_show(self):
        index = 1
        self.countBorders()
        print(self.border)

        with_data = False
        datas = None

        self.tree = {}
        addTree = True # Добавлять ли элемент?

        # Если нам передали список, а не дикт
        if isinstance(self.menu_items, list):
            for line in self.menu_items:
                # Элмент, меньший по длине чем ограничения
                if len(line) < self.max_line:   
                    print(f"| {index}. {line}".ljust(self.max_item_length+5) + ' |')
                    print(self.border)
                    index += 1
                # Элмент, больший по длине чем ограничения
                else:
                    lines = [f" {index}. {line[i:i+self.max_line]}" for i in range(0, len(line), self.max_line)]
                    for line in lines:
                        formatted_line = f"| {line.ljust(self.max_item_length+3)} |"
                        print(formatted_line)
                    print(self.border)
                    index += 1
            if self.null:
                print("|" + f" 0. Назад ".rjust(self.max_item_length+5) + "|")
                print(self.border)
            # print(f"| {index} ".ljust(self.max_item_length+5) + ' |')
            # print(self.border)
        
        # Если нам передали словарь
        else:
            # Печатаем список меню
            for item_name, v in self.menu_items.items():
                # Если в именеи None, то это что-то системное... бэкдур
                if item_name is None:
                    addTree = False 

                # Моя кастомная опция
                elif isinstance(item_name, O):
                    index, addTree = item_name.print_item(index, self.max_item_length, self.border)
                    self.before = item_name.addBefore


                # Элмент, меньший по длине чем ограничения
                elif len(item_name) < self.max_line:   
                    print(f"| {index}. {item_name}".ljust(self.max_item_length+5) + ' |')
                    print(self.border)
                    index += 1
                # Элмент, больший по длине чем ограничения
                else:
                    lines = [f" {index}. {item_name[i:i+self.max_line]}" for i in range(0, len(item_name), self.max_line)]
                    for line in lines:
                        formatted_line = f"| {line.ljust(self.max_item_length+3)} |"
                        print(formatted_line)
                    print(self.border)
                    index += 1

                if isinstance(v, MenuSett) and item_name is None:
                    addTree=False

                if addTree:
                    self.tree[item_name] = v
                
        if self.parent_menu is None:
            pass
        else:
            print("|" + f" 0. Назад ".rjust(self.max_item_length+5) + "|")
            print(self.border)


    # # Проблема пре-инициализации в тексте (в ключах) меню.
    # # Меняет '_' на return функции. для 
    # def lst_factory(self, lst):
    #     '''
    #         lst = [
    #             str,
    #             str,
    #             str,
    #         ]
    #     '''
    #     rez_lst = []
    #     # Рассматриваем каждую строчку
    #     for strring in lst:
    #         rez_str = ''
    #         # Теперь проверяем каждый символ в строке
    #         for ch in strring:
    #             # функцию выполняем
    #             if callable(ch):
    #                 input(f'Найден вызываемый обьект!!!! rez_str = {rez_str}, ch() = {ch()}')
    #                 rez_str = rez_str + ch()
    #             # У нас символ, пропускаем его
    #             else:
    #                 rez_str = rez_str + ch
    #         rez_lst.append(rez_str)
    #     return rez_lst


    def display_menu(self):
        while True:
            self.countBorders()
            self.clear_screen()

            msg(self.before)

            # Подпись "меню" и хлебные крошки
            if not color:
                self.msg('Цветовая схема не загружена. Для загрузки установите colorama')
            print("+" + " Меню ".center(self.max_item_length+5, '=') + "+")
            if color:
                print("|" + Fore.RED + " > ".join(self.path).center(self.max_item_length+5, ' ') + Style.RESET_ALL + "|")
            else:
                print("|" + " > ".join(self.path).center(self.max_item_length+5) + "|")

            # Печать меню
            self.show_menu_items()
            # Выбор элемента меню
            lst_item = self.choice_menu_items()

            # if lst_item: return lst_item
            if isinstance(self.menu_items, list): return lst_item

            # elif self.rescreen: 
            #     self.do_rescreen()


    def do_rescreen(self):
        self.rescreen=False
        self.display_menu()


    def check_parent(self):
        if self.parent_menu is None:
            return
        else:
            return self.parent_menu.display_menu()


    def show_menu_items_simple(self):
        index = 1
        for item_name, item_value in self.menu_items.items():
            print(f"{index}. {item_name}")
            index += 1
        if self.parent_menu is None:
            pass
        else:
            print(f"0. Назад")


    def show_menu_items(self):
        index = 1
        self.countBorders()
        print(self.border)

        with_data = False
        datas = None

        self.tree = {}
        addTree = True # Добавлять ли элемент?

        # Если нам передали список, а не дикт
        if isinstance(self.menu_items, list):
            for line in self.menu_items:
                # Элмент, меньший по длине чем ограничения
                if len(line) < self.max_line:   
                    print(f"| {index}. {line}".ljust(self.max_item_length+5) + ' |')
                    print(self.border)
                    index += 1
                # Элмент, больший по длине чем ограничения
                else:
                    lines = [f" {index}. {line[i:i+self.max_line]}" for i in range(0, len(line), self.max_line)]
                    for line in lines:
                        formatted_line = f"| {line.ljust(self.max_item_length+3)} |"
                        print(formatted_line)
                    print(self.border)
                    index += 1
            if self.null:
                print("|" + f" 0. Назад ".rjust(self.max_item_length+5) + "|")
                print(self.border)
            # print(f"| {index} ".ljust(self.max_item_length+5) + ' |')
            # print(self.border)
        # Если нам передали словарь
        else:
            # Печатаем список меню
            for item_name, v in self.menu_items.items():
                # Если в именеи None, то это что-то системное... бэкдур
                if item_name is None:
                    addTree = False # Не будем выбирать и печатать
                    # if isinstance(v, MenuSett):
                    #     if v.msg and self.before is not None:
                    #         self.before = v.msg
                    #         self.rescreen=True


                # Моя кастомная опция
                elif isinstance(item_name, O):
                    index, addTree = item_name.print_item(index, self.max_item_length, self.border)


                # Элмент, меньший по длине чем ограничения
                elif len(item_name) < self.max_line:   
                    print(f"| {index}. {item_name}".ljust(self.max_item_length+5) + ' |')
                    print(self.border)
                    index += 1
                # Элмент, больший по длине чем ограничения
                else:
                    lines = [f" {index}. {item_name[i:i+self.max_line]}" for i in range(0, len(item_name), self.max_line)]
                    for line in lines:
                        formatted_line = f"| {line.ljust(self.max_item_length+3)} |"
                        print(formatted_line)
                    print(self.border)
                    index += 1

                if isinstance(v, MenuSett) and item_name is None:
                    addTree=False

                if addTree:
                    self.tree[item_name] = v
                
        if self.parent_menu is None:
            pass
        else:
            print("|" + f" 0. Назад ".rjust(self.max_item_length+5) + "|")
            print(self.border)

            
    def choice_menu_items(self):
        # Выбор меню
        ext = '(или несколько, разделяя пробелом) ' if self.multilist else ''
        choice = input(f"Введите номер пункта {ext}(q для выхода): ")

        # Пробуем обработать, если это несколько чисел
        try:
            if self.multilist: 
                rez = []
                list_items = choice.split(' ')
                for item in list_items:
                    # input(f'int(item) = {int(item)}, len(self.menu_items) = {len(self.menu_items)}')
                    if int(item) == 0 and self.null: return {'ok': False} 
                    elif 1 <= int(item) <= len(self.menu_items):
                        # input(f'rez was appended!')
                        rez.append(self.menu_items[int(item)-1])
                # input(f'rez --- {rez}')
                return {'ok': True, 'rez': rez}
        except: 
            self.before = 'Ошибка в последовательности. Попройбуйте еще раз'
        
        # Выход
        if choice == 'q':
            ans = input("\nDo you really want to go out (Y/n) ").upper()
            if ans == "Y":
                exit()
        
        # Если это число ...
        elif choice.isdigit():
            choice = int(choice)
            menu_items = {}
            # Это исключит нумерацию элементов, которые нумеровать не надо (_)
            for k,v in self.tree.items():
                if k[0] != '_':
                    menu_items[k] = v

            # Список обрабатываем отдельно. При выборе элемента - вернуть его..
            if isinstance(self.menu_items, list):
                # input('Выбор в списке...')
                if 1 <= choice <= len(self.menu_items):
                    if self.returnkey:
                        return choice
                    else:
                        return self.menu_items[choice-1]
                elif int(choice) == 0:
                    # input(f'choice = {choice}')
                    return False
                else:
                    input("Неверный выбор. Нажмите Enter для продолжения.")


            # Выбран легитимный вариант меню
            elif 1 <= choice <= len(menu_items):
                selected_key = list(menu_items.keys())[choice - 1]
                selected_item = list(menu_items.values())[choice - 1]

                if selected_key[0] == '_':
                    pass

                # Если значение - мое кстомное, то... (Точно не словарь)
                elif isinstance(selected_item, V):
                    item = selected_item
                    # Если это функция
                    if item.f:
                        if debug: print(f'item.f = {item.f}, ')
                        if debug: print(f'item.attr = {item.attr}, ')
                        
                        logger.debug(f'choice_menu_items: Выполняется функция! Текущий self.tmp = {self.tmp}')

                        if item.attr is not None: item.f(item.attr)
                        else: item.f()
                        if debug: input('...')
                        logger.debug(f"choice_menu_items: Функция выполнена, self.tmp = {self.tmp}")
                        logger.debug(f"choice_menu_items: Очистка tmp...")
                        self.tmp = []
                        logger.debug(f"choice_menu_items: После очистки: self.tmp = {self.tmp}")

                        input(f"Функция выполнена. Нажмите Enter для продолжения...")
                    # Если есть и словарь и сообщение, то значит что пробрасываем before...
                    if item.d and item.msg:
                        sub_menu = ConsoleMenu(item.d, self, self.path+[str(selected_key),], before=item.msg)
                        sub_menu.display_menu()

                    # Если это список
                    if selected_item.lst:
                        if selected_item.lst_multy:
                            items = ConsoleMenu(selected_item.lst).display_menu()
                            # print(items)
                            # input('prinkjkj')
                            # selected_item.var['testing'] = items


                    # Нужно ли возвращаться на экран назад?
                    if item.back_screen: self.check_parent()

                # Элемент - словарь - подменю
                elif isinstance(selected_item, dict):
                    sub_menu = ConsoleMenu(selected_item, self, self.path+[str(selected_key),], before=self.before)
                    sub_menu.display_menu()

                # Элемент - кортеж - функция с параметрами
                elif isinstance(selected_item, tuple):
                    '''
                        0 - сама функция
                        1 - параметры
                        2 - нужно ли возвращать на экран назад (0 - нет, 1 - да)
                    '''
                    
                    # Если есть пармаетры - то с параметрами
                    if selected_item[1] is None:
                        selected_item[0]()                            
                    else:
                        selected_item[0](*selected_item[1])
                    
                    if debug: input('...')

                    # Нужно ли возвращаться на экран назад?
                    if selected_item[2] == 1:
                        self.check_parent()
                    else:
                        self.display_menu()

                # Элемент - MenuSett - тут уже разное....
                elif isinstance(selected_item, MenuSett):
                    # Если есть сообщение - печатеам
                    if selected_item.msg:
                        self.before = selected_item.msg
                        

                # Элмент - функция - выполняем
                elif callable(selected_item):
                    input(f'Выполняется функция!')
                    selected_item(self)
                    input("Функция выполнена. Нажмите Enter для продолжения... ")
                    self.check_parent()
                    

            elif choice == 0 and self.null:
                self.check_parent()
            else:
                input("Неверный выбор. Нажмите Enter для продолжения.")
        
        else:
            input("Введено неккоректное значение. Нажмите Enter для продолжения.")


    def countBorders(self):
        '''
            max_current - самая длинная длина элемента в списке подменю
            Если она меньше чем длина хлебных крошек (lengh_menu), то выставляется 
            длинна этих крошек, чтобы меню выглядело целостным
            
            max_line - ограничение вывода, настройка в init
            
            max_item_length - итоговая длинна линии. Равняется текущей, если текущаяя меньше лимита
            и лимитом, если она больше лимита

            border - отрисовка 
        '''
        lst = []
        if isinstance(self.menu_items, list):
            for i in self.menu_items:
                lst.append(len(i))
        else:
            for i in self.menu_items.keys():
                if i is not None:
                    lst.append(len(i))
        # self.max_current = max(len(item_name) for item_name in self.menu_items.keys())
        self.max_current = max(lst)
        lengh_menu = len(" > ".join(self.path))
        self.max_current = self.max_current if self.max_current > lengh_menu else lengh_menu
        self.max_item_length = self.max_current if self.max_current < self.max_line else self.max_line
        self.border = "+" + "-" * (self.max_item_length + 5) + "+"


    def msg(self, txt):
        border = "+" + "-" * (self.max_item_length + 4) + "+"
        
        lines = [txt[i:i+self.max_item_length] for i in range(0, len(txt), self.max_item_length)]
        
        print(border)
        for line in lines:
            formatted_line = f"|  {line.ljust(self.max_item_length)}  |"
            print(formatted_line)
        print(border)






if __name__=='__main__':
    # Пример использования
    def action1():
        print("Вы выбрали Действие 1.")

    def sub_menu_action_2_1():
        print("Вы выбрали Действие 2.1.")

    def sub_menu_action_2_2():
        print("Вы выбрали Действие 2.2.")

    sub_menu_items = {
        'Действие 2.1': sub_menu_action_2_1,
        'Действие 2.2': sub_menu_action_2_2
    }

    main_menu_items = {
        'Действие 1': action1,
        'Действие 2': sub_menu_items,
        'Действие 3': {
            'Действие 3.1': None,
            'Действие 3.2': None,
            'Действие 3.3': {
                'Действие 3.3.1': None,
                'Действие 3.3.2': None
            }
        }
    }

    main_menu = ConsoleMenu(main_menu_items)
    main_menu.display_menu()