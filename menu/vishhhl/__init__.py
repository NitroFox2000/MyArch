"""
vishhhl version 0.3.2
This library makes it easy to create a beautiful visual menu in the console. Based on the VisualMenu library.
Email - sw3atyspace@gmail.com
Discord Server - https://discord.com/invite/jchJKYqNmK
Youtube - https://www.youtube.com/@sw3aty702
"""

version = "0.3.2"

function = type(lambda: 0)

from . import Core
from . import Widgets
