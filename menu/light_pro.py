import os
from utils import msg

debug = 1

try:
    from colorama import init
    init(autoreset=True)
    from colorama import Fore, Back, Style
    color=True
    print('Цветовая схема не загружена. Для загрузки установите colorama')
except:
    print('Цветовая схема загружена.')
    color=False



class MenuSett:
    def __init__(self, msg=None, data_info=None):
        self.msg = msg,
        self.data_info = data_info



# Класс-значения
class V:
    def __init__(self, title, f, var=False, none=False):
        self.title = title
        self.f = f
        self.var = var
        self.none = none



# Класс-ключи
class O:
    def __init__(self, title, var, blank=False, color=None):
        self.title = title
        self.var = var
        self.blank = blank
        self.color = color


# Класс меню - главный
class ConsoleMenu:
    def __init__(self, menu_items, parent_menu=None, path=['Главное меню',], before=None):
        self.tree = menu_items
        self.path = path
        self.parent_menu = parent_menu
        self.before = before

        self.max_line = 80          # Максимальная длинна меню (ограничение)
        self.max_current = 0        # Максимальная длинна строки текущего подменю
        self.max_item_length = 0    # Максимальная длинна 
        self.border = ''


    def clear_screen(self):
        os.system('cls' if os.name == 'nt' else 'clear')


    def display_menu(self):
        while True:
            self.countBorders()
            self.clear_screen()

            msg(self.before)

            # Подпись меню и хлебные крошки
            if not color:
                self.msg('Цветовая схема не загружена. Для загрузки установите colorama')
            print("+" + " Меню ".center(self.max_item_length+5, '=') + "+")
            if color:
                print("|" + Fore.RED + " > ".join(self.path).center(self.max_item_length+5, ' ') + Style.RESET_ALL + "|")
            else:
                print("|" + " > ".join(self.path).center(self.max_item_length+5) + "|")

            # Элементы меню
            self.show_menu_items()
            
            # Выбор меню
            choice = input("Введите номер пункта (q для выхода): ")
            # Выход
            if choice == 'q':
                ans = input("\nDo you really want to go out (Y/n) ").upper()
                if ans == "Y":
                    exit()
            # Если это число ...
            elif choice.isdigit():
                choice = int(choice)
                menu_items = {}
                for k,v in self.tree.items():
                    if k[0] != '_':
                        menu_items[k] = v

                # Выбран легитимный вариант меню
                if 1 <= choice <= len(menu_items):
                    selected_key = list(menu_items.keys())[choice - 1]
                    selected_item = list(menu_items.values())[choice - 1]

                    if selected_key[0] == '_':
                        pass

                    # Элмент - функция - выполняем
                    elif callable(selected_item):
                        selected_item()
                        input("Нажмите Enter для продолжения...")
                        self.check_parent()

                    # Элемент - словарь - подменю
                    elif isinstance(selected_item, dict):
                        sub_menu = ConsoleMenu(selected_item, self, self.path+[selected_key,], before=self.before)
                        sub_menu.display_menu()

                    # Элемент - кортеж - функция с параметрами
                    elif isinstance(selected_item, tuple):
                        '''
                            0 - сама функция
                            1 - параметры
                            2 - нужно ли возвращать на экран назад (0 - нет, 1 - да)
                        '''
                        
                        # Если есть пармаетры - то с параметрами
                        if selected_item[1] is None:
                            selected_item[0]()                            
                        else:
                            selected_item[0](*selected_item[1])
                        
                        if debug: input('...')

                        # Нужно ли возвращаться на экран назад?
                        if selected_item[2] == 1:
                            self.check_parent()
                        else:
                            self.display_menu()

                    # Элемент - MenuSett - тут уже разное....
                    elif isinstance(selected_item, MenuSett):
                        # Если есть сообщение - печатеам
                        if selected_item.msg:
                            self.before = selected_item.msg
                            # self.msg(selected_item.msg)
                        

                elif choice == 0:
                    self.check_parent()
                else:
                    input("Неверный выбор. Нажмите Enter для продолжения.")
            else:
                input("Неверный выбор. Нажмите Enter для продолжения.")


    def check_parent(self):
        if self.parent_menu is None:
            return
        else:
            return self.parent_menu.display_menu()


    def show_menu_items_simple(self):
        index = 1
        for item_name, item_value in self.tree.items():
            print(f"{index}. {item_name}")
            index += 1
        if self.parent_menu is None:
            pass
        else:
            print(f"0. Назад")


    def show_menu_items(self):
        index = 1
        self.countBorders()
        print(self.border)

        with_data = False
        datas = None
        
        # Печатаем список меню
        for item_name, v in self.tree.items():
            # Если в именеи None, то это что-то системное... бэкдур
            if item_name is None:
                pass
                # Если это, то отображать и текст, и сами значения
                if v.data_info:
                    with_data = True
                    # print(f'type = {type(v.data_info)}, v.data_info = {v.data_info}')
                    datas = v.data_info

            # Если есть управляющий символ, то просто вывести без номера для выбора
            elif item_name[0] == '_':
                print("|" + f"{item_name[1:]}".center(self.max_item_length+5) + '|')
                print(self.border)

            # Если есть точка, то к этому сиволу добавить значение
            elif item_name[0] == '.':
                print(f"| {index}. " 
                      + f"{item_name[1:]}".ljust(int((self.max_item_length+1)/2)) 
                      + '=' 
                      + f'{v[3]} '.rjust(int((self.max_item_length)/2)) 
                      + '|')
                print(self.border)
                index += 1

            # Элмент, меньший по длине чем ограничения
            elif len(item_name) < self.max_line:   
                print(f"| {index}. {item_name}".ljust(self.max_item_length+5) + ' |')
                print(self.border)
                index += 1
            # Элмент, больший по длине чем ограничения
            else:
                lines = [f" {index}. {item_name[i:i+self.max_line]}" for i in range(0, len(item_name), self.max_line)]
                for line in lines:
                    formatted_line = f"| {line.ljust(self.max_item_length+3)} |"
                    print(formatted_line)
                print(self.border)
                index += 1
        if self.parent_menu is None:
            pass
        else:
            print("|" + f" 0. Назад ".rjust(self.max_item_length+5) + "|")
            print(self.border)


    def countBorders(self):
        '''
            max_current - самая длинная длина элемента в списке подменю
            Если она меньше чем длина хлебных крошек (lengh_menu), то выставляется 
            длинна этих крошек, чтобы меню выглядело целостным
            
            max_line - ограничение вывода, настройка в init
            
            max_item_length - итоговая длинна линии. Равняется текущей, если текущаяя меньше лимита
            и лимитом, если она больше лимита

            border - отрисовка 
        '''
        lst = []
        for i in self.tree.keys():
            if i is not None:
                lst.append(len(i))
        # self.max_current = max(len(item_name) for item_name in self.tree.keys())
        self.max_current = max(lst)
        lengh_menu = len(" > ".join(self.path))
        self.max_current = self.max_current if self.max_current > lengh_menu else lengh_menu
        self.max_item_length = self.max_current if self.max_current < self.max_line else self.max_line
        self.border = "+" + "-" * (self.max_item_length + 5) + "+"


    def msg(self, txt):
        border = "+" + "-" * (self.max_item_length + 4) + "+"
        
        lines = [txt[i:i+self.max_item_length] for i in range(0, len(txt), self.max_item_length)]
        
        print(border)
        for line in lines:
            formatted_line = f"|  {line.ljust(self.max_item_length)}  |"
            print(formatted_line)
        print(border)






if __name__=='__main__':
    # Пример использования
    def action1():
        print("Вы выбрали Действие 1.")

    def sub_menu_action_2_1():
        print("Вы выбрали Действие 2.1.")

    def sub_menu_action_2_2():
        print("Вы выбрали Действие 2.2.")

    sub_menu_items = {
        'Действие 2.1': sub_menu_action_2_1,
        'Действие 2.2': sub_menu_action_2_2
    }

    main_menu_items = {
        'Действие 1': action1,
        'Действие 2': sub_menu_items,
        'Действие 3': {
            'Действие 3.1': None,
            'Действие 3.2': None,
            'Действие 3.3': {
                'Действие 3.3.1': None,
                'Действие 3.3.2': None
            }
        }
    }

    main_menu = ConsoleMenu(main_menu_items)
    main_menu.display_menu()