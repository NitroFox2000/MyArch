import os
import sys
import time

def list_files():
    files = os.listdir()
    for i, file in enumerate(files):
        print(f"{i + 1}. {file}")
    return files

def read_file(filename):
    try:
        with open(filename, "r", encoding="utf-8") as file:
            return file.read()
    except FileNotFoundError:
        print(f"Файл '{filename}' не найден.")
        sys.exit(0)
    except Exception as e:
        print(f"Произошла ошибка при чтении файла '{filename}': {e}")
        return False


def loop(name):
    content = read_file(name)
    print(content)
    print("=" * 40)
    previous_content = content
    while True:
        time.sleep(0.5)
        new_content = read_file(name)
        if new_content != previous_content:
            print("Изменения:")
            diff = new_content[len(previous_content):]
            print(diff)
            previous_content = new_content


def main():
    name = 'logfile.txt'
    
    try:
        if name:
            loop(name)
        else:
            files = list_files()
            while True:
                file_number = int(input("Выберите номер файла (или нажмите Ctrl+C для выхода): "))
                if 1 <= file_number <= len(files):
                    selected_file = files[file_number - 1]
                    loop(selected_file)
                else:
                    print("Неверный номер файла. Попробуйте еще раз.")

    except KeyboardInterrupt:
        print("Программа завершена.")
        sys.exit(0)
    except ValueError:
        print("Введите корректный номер файла.")
    except Exception as e:
        print(f"Произошла ошибка: {e}")


        


if __name__ == "__main__":
    main()
