# -*- coding: utf-8 -*-
from menu.light import *
from utils import *
# from ArchInstall.version2.archInstall import archInstall
# from ArchInstall.version2.archfast import main
from ArchInstall import version1
from ArchInstall import version2
from ArchInstall import version3





def info():
    msg([
        'Author: Maks',
        'Version: 1.3',
        'Description: Проект разрабатывается, как единая точка входа для доступа ко всем скриптам.'
    ], multi=True)


main_menu_items = {
    'Install ArchLinux (v1)': version1.archInstall.archInstall,
    'Install ArchLinux (v2)': version2.archInstall.archInstall,
    'Install ArchLinux (v3)': version3.archInstall.archInstall,
    'Fast install Arch (json)': V(version2.archfast.main),
    'Settings Arch (dot files)': None,
    'Settings': None,
    'Info (в разработке)': V(info),
    # 'Загрузить современное меню (В разработке)': archFull.start,
}

# main_menu = ConsoleMenu(main_menu_items, before=sett)
main_menu = ConsoleMenu(main_menu_items)

def liux_before():
    os.system('loadkeys ru')
    os.system('setfont cyr-sun16')

def start():
    if lin: liux_before()
    main_menu.display_menu()

if __name__=='__main__':
    try:
        start()
    except KeyboardInterrupt:
        print("\n\n\t\t Программа завершена.")
        sys.exit(0)


'''
archInstall = {

}
'''