
from menu.vishhhl.addons.Menu import *
import colorama

clrm = colorama.Fore


class Menu(mOption):
    login = None

    def __init__(self,
                 title: str = None,
                 desc: str = None,
                 text: str = None,
                 comment: str = None):
        self.org_title = title
        desc = desc if desc else f"This is the {colorama.Fore.MAGENTA}vishhhl{colorama.Style.RESET_ALL}!"
        text = text if text else title
        comment = comment if comment else desc

        super().__init__(text, self, comment, title=title, desc=desc)
        self.symbol = "\n"

    def update(self):
        if self.login:
            self.changeTitle(f"{self.org_title} | Authorized: {self.login}")
        else:
            self.changeTitle(self.org_title)


class clsSetting(Menu):
    def __init__(self):
        super().__init__("Настроить цвет",
                         comment="Customize button color")
        self.addOption(mLink("Red", link=self.changeColor, args=[0]),
                       mLink("Green", link=self.changeColor, args=[1]),
                       mLink("Cyan", link=self.changeColor, args=[2]),
                       mLink("Back", link=self.disable))

    def changeColor(self, index):
        for i in menMain.func_list + self.func_list:
            i.color = (clrm.RED, clrm.GREEN, clrm.CYAN)[index]

    def on_selected(self):
        ret = super().on_selected()
        # input(ret)
        return ret
    


class clsMain(Menu):
    def __init__(self):
        super().__init__("Menu")


        self.addOption(
            mLink(text="Установка ArchLinux", link=self.free),
            mLink(text="Быстрая установка Arch (json)", link=self.free),
            mLink(text="Настройка системы (dot files)", link=self.free),
            mLink(text="Инфо о системе", link=self.free),
            clsSetting(), 
            mLink(text="Старое меню", link=self.sMenu),
            mLink(text="Выход", link=self.fQuit),
        )

    def fQuit(self):
        ans = input("\nDo you really want to go out (Y/n) ").upper()
        if ans == "Y":
            exit()

    def sMenu(self):
        import arch
        arch.start()

    def free(self):
        pass





menMain = clsMain()
def start():
    menMain.enable()