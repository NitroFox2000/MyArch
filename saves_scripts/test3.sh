"mkfs.ext4 /dev/sda1 -L root
mount /dev/sda1 /mnt
pacman -S reflector --noconfirm
reflector --verbose -l 50 -p http --sort rate --save /etc/pacman.d/mirrorlist
reflector --verbose -l 15 --sort rate --save /etc/pacman.d/mirrorlist
pacstrap /mnt base dhcpcd linux linux-headers which netctl inetutils base-devel  wget linux-firmware  nano python3 git wpa_supplicant dialog
genfstab -pU /mnt >> /mnt/etc/fstab

 arch-chroot /mnt sh -c \"$(echo arch > /etc/hostname
 ln -sf /usr/share/zoneinfo/Europe/Moscow /etc/localtime
 pacman -S grub grub-customizer os-prober  --noconfirm
 echo \"GRUBsdaDISABLEsdaOSsdaPROBER=false\" >> /etc/default/grub
 grub-install /dev/sda
 grub-mkconfig -o /boot/grub/grub.cfg
 mkinitcpio -p linux
 echo '[multilib]' >> /etc/pacman.conf
 echo 'Include = /etc/pacman.d/mirrorlist' >> /etc/pacman.conf
 pacman -Sy xorg-server xorg-drivers xorg-xinit virtualbox-guest-utils --noconfirm
 pacman -Sy networkmanager networkmanager-openvpn network-manager-applet ppp --noconfirm
 systemctl enable NetworkManager.service
 systemctl enable dhcpcd.service
 pacman -Sy pulseaudio-bluetooth alsa-utils pulseaudio-equalizer-ladspa   --noconfirm
 systemctl enable bluetooth.service
 pacman -Sy exfat-utils ntfs-3g   --noconfirm
 pacman -Sy unzip unrar lha ark file-roller p7zip unace lrzip --noconfirm
 )\" 
"

