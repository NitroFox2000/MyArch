import logging

# Создать объект логгера
logger = logging.getLogger('my_logger')

# Установить уровень логирования (можете выбрать нужный уровень)
logger.setLevel(logging.DEBUG)

# Создать обработчик для записи логов в файл
file_handler = logging.FileHandler('logfile.txt', encoding='utf-8')

# Создать форматтер для логов (опционально)
formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
file_handler.setFormatter(formatter)

# Добавить обработчик к логгеру
logger.addHandler(file_handler)


"""
logger.debug('Это сообщение отладки')
logger.info('Это информационное сообщение')
logger.warning('Это предупреждение')
logger.error('Это ошибка')
logger.critical('Это критическая ошибка')

"""